using UnityEngine;
using System.Collections;

public class PauseMenu : GUIMenu {

	
	public PauseMenu()
		: base()
	{
		initMenu();
	}
	
	private void initMenu()
	{
		float buttonHeight = 100f;
		float buttonWidth = 680f;
		
		GUIManager gui = GameObject.Find("Core").GetComponent<GUIManager>();
		
		_items.Add(new GUIButton("Restart", buttonWidth, buttonHeight, new GUIButton.SetterBool(gui.setRetryGame)));
		_items.Add(new GUIButton("Back to main menu", buttonWidth, buttonHeight,
			new GUIButton.SetterBool(gui.setReturnToMenu)));
	}
	
	public override void draw ()
	{
		base.draw ();
		
		float areaWidth = 1000f * GUIManager.widthCoef;
		float areaHeight = 600f * GUIManager.heightCoef;
		float areaX = Screen.width/2f - areaWidth/2f;
		float areaY = Screen.height/2f - areaHeight/2f;
		
		GUILayout.BeginArea(new Rect(areaX, areaY, areaWidth, areaHeight));
		GUILayout.BeginVertical();
		
		GUILayout.FlexibleSpace();
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label("Game paused", "deathFont");
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.FlexibleSpace();
		
		foreach(GUIItem itm in _items)
		{
			itm.draw();
			GUILayout.FlexibleSpace();
		}
		
		GUILayout.FlexibleSpace();
		
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

}
