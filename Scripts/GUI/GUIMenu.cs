using UnityEngine;
using System.Collections.Generic;

public class GUIMenu {
	
	public static event System.Action unselectionEvent;
	
	protected List<GUIItem> _items;
	protected Vector2 _scroolPos = Vector2.zero;
	
	public GUIMenu()
	{
		_items = new List<GUIItem>();
		unselectionEvent += resetSelection;
	}

	public virtual void draw()
	{
		
	}
	
	public virtual void update(float deltaTime)
	{
		
		float UIHoriz = InputManager.input.getUIHorizontal();
		if(UIHoriz != 0f)
		{
			int i = getSelectedIndex();
			if(i != -1)
			{
				_items[i].forceValChange(UIHoriz);	
			}
		}
		else
		{
			float UIVertic = InputManager.input.getUIVertical();
			if(UIVertic != 0f)
			{
				int i = getSelectedIndex();
				UIVertic = UIVertic < 0f ? -1f : 1f;
				i += (int)UIVertic * -1; //verticalInvert
				if(i >= 0 && i <_items.Count)
				{
					resetSelection();
					_items[i].setSelected(true);
					Rect pos = _items[i].getRect();
					_scroolPos.x = pos.x;
					_scroolPos.y = pos.y;
				}
			}	
		}
		
		bool valid = InputManager.input.getUIValidate();
		if(valid)
		{
			int i = getSelectedIndex();
			if(i >= 0 && i < _items.Count)
			{
				_items[i].forceAction();	
			}
		}
	}
	
	protected int getSelectedIndex()
	{
		int selected = -1;
		
		int i = 0;
		foreach(GUIItem itm in _items)
		{
			if(itm.isSelected())
			{
				selected = 	i;
			}
			++i;
		}
		
		return selected;
	}
	
	public static void fireUnselection()
	{
		if(unselectionEvent != null)
		{
			unselectionEvent();	
		}
	}
	
	public void resetSelection()
	{
		foreach(GUIItem itm in _items)
		{
			itm.setSelected(false);
		}
		_scroolPos = Vector2.zero;
	}
}
