using UnityEngine;
using System.Collections.Generic;

public class CreatureSoundHandler : MonoBehaviour {

	public List<SoundManager.clipAndVolume> baseSounds;
	public List<SoundManager.clipAndVolume> fleeSounds;
	public List<SoundManager.clipAndVolume> deathSounds;

	
	public void playFleeSound(bool forcePlay = false)
	{
		int i = Random.Range(0, fleeSounds.Count - 1);
		SoundManager.askForSoundPlay(transform, fleeSounds[i], forcePlay);
	}
	
	public void playDeathSound(bool forcePlay = false)
	{
		int i = Random.Range(0, deathSounds.Count - 1);
		SoundManager.askForSoundPlay(transform, deathSounds[i], forcePlay);
	}
	
	public void playBaseSound(bool forcePlay = false)
	{
		int i = Random.Range(0, baseSounds.Count - 1);
		if(i < baseSounds.Count)
		{
			SoundManager.askForSoundPlay(transform, baseSounds[i], forcePlay);
		}
	}
}
