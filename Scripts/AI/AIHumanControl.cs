using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// AI human control.
/// </summary>
public class AIHumanControl : AICreatureControl {
	

	private DoorHandler _nextReachedDoor;
	
	protected override void collisionOnPath(RaycastHit hit)
	{
		_path = null;
		_currentPathTarget = 0;
		newTarget();	
	}
	
	public override void newTarget ()
	{
		//base.newTarget ();
		
		if(_path == null || _currentPathTarget >= _path.Count - 1)
		{
			if(_path != null && _nextReachedDoor != null && !_nextReachedDoor.enabled
				&& (_nextReachedDoor.transform.position - transform.position).magnitude < actDistance)
			{
				_nextReachedDoor.enabled = true;
				_nextReachedDoor.setOccupant(transform);
			}
			
			if(!_waitingForPath)
			{
				GameObject AI = GameObject.Find("AI");
				MapGenerator map = AI.GetComponent<MapGenerator>();
				List<Transform> houses = new List<Transform>();
				houses.AddRange(map.getHouses());
				
				//find a house to reach
				int i = Random.Range(0, houses.Count - 1);
				while(houses[i].GetComponent<DoorHandler>().enabled)//if door is closed
				{
					i = Random.Range(0, houses.Count - 1);
				}
				_nextReachedDoor = houses[i].GetComponent<DoorHandler>();
				
				Vector3 end = _nextReachedDoor.housePos.position;
				Vector3 start = transform.position;
				_path = null;
				
				//ask the scheduler for a path
				Scheduler.aksForPath( start, end, this);
				_waitingForPath = true;
				_currentPathTarget = 0;
			}
			
			if(_path != null)
			{
				_target = _path[_currentPathTarget];
			}
			else
			{
				base.newTarget();
			}	
		}
		else
		{
			_currentPathTarget = getNextReachable(_path, _currentPathTarget);
			_target = _path[ _currentPathTarget];
			_target.y = 1f;
		}
	}
	
	private void trySetDoorState(bool open)
	{
		if(_nextReachedDoor != null)
		{
			_nextReachedDoor.enabled = !open;
		}
	}
			
}
