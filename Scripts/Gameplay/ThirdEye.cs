using UnityEngine;
using System.Collections;

public class ThirdEye : Power {

	private GameObject _player;
	private GameObject _eyeContainer;
	private GameObject _playerCam;
	private PlayerControl _control;
	
	
	public ThirdEye(float duration, float cooldown)
		: base(duration, cooldown)
	{
		Debug.Log("third eye init");
		_player = GameObject.Find("player");
		_eyeContainer = GameObject.Find("EyeContainer");
		_eyeContainer.SetActive(false);
		_playerCam = GameObject.Find("player camera");
		
		_control = _player.GetComponent<PlayerControl>();
		
		_type = Player.PowerType.THIRD_EYE;
	}
	
	public override bool activate()
	{
		if(base.activate())
		{	
			_playerCam.SetActive(false);
			_control.Controlled = false;
			_eyeContainer.SetActive(true);
			
			
			Vector3 eyePos = _player.transform.position;
			eyePos.y += 5f;
			eyePos.z += 5f;
			_eyeContainer.transform.position = eyePos;
			
			Vector3 rotation = _player.transform.rotation.eulerAngles;
			rotation.x = 0;
			rotation.z = 0;
			
			_eyeContainer.transform.rotation = _player.transform.rotation;
			
			return true;
		}
		
		return false;
	}
	
	
	protected override void hide()
	{
		_control.Controlled = true;
		_eyeContainer.SetActive(false);
		_playerCam.SetActive(true);
	}
}
