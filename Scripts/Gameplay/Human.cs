using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Human : Creature {

	
	public override void Awake()
	{
		_type = Type.Human;
		_preyedTypes = new List<Type>();
		_eatenScale = new Vector3(1f, 0.1f, 1f);
	}
}
