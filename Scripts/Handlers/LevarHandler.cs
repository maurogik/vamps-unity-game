using UnityEngine;
using System.Collections.Generic;

public class LevarHandler : MonoBehaviour {
	
	public enum axis{
		X, Y, Z	
	}
	
	public LevarHandler secondLevar;
	public List<Transform> levarSticks;
	public List<Transform> bridges;
	
	public float startLevarRot = -30f;
	public float endLevarRot = 30f;
	public float levarAnimDuration = 1f;
	
	public float startBridgeRot = 0f;
	public float endBridgeRot = 90f;
	public float bridgeAnimDuration = 5f;
	public axis bridgeRotationAxis;
	
	public bool switchBypas = false;
	
	public SoundManager.clipAndVolume leverSound;
	
	private bool bridgeDown = true;
	private float _timer = 0f;
	
	// Use this for initialization
	void Start ()
	{
		_timer = bridgeAnimDuration;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(switchBypas)
		{
			switchBypas = false;
			switchLevar();
		}
		
		_timer += Time.deltaTime;
		
		float levarStart, levarEnd, bridgeStart, bridgeEnd;
		levarStart = bridgeDown ? endLevarRot : startLevarRot;
		levarEnd = bridgeDown ? startLevarRot : endLevarRot;
		bridgeStart = bridgeDown ? endBridgeRot : startBridgeRot;
		bridgeEnd = bridgeDown ? startBridgeRot : endBridgeRot;
		
		float levarRot = Mathf.Lerp(levarStart, levarEnd, _timer/levarAnimDuration);
		float bridgeRot = Mathf.Lerp(bridgeStart, bridgeEnd, _timer/bridgeAnimDuration);
		
		foreach(Transform levar in levarSticks)
		{
			Vector3 rot = levar.localRotation.eulerAngles;
			rot.x = levarRot;
			levar.localRotation = Quaternion.Euler(rot);
		}
		
		foreach(Transform bridge in bridges)
		{
			Vector3 rot = bridge.localRotation.eulerAngles;
			
			switch(bridgeRotationAxis)
			{
			case axis.X :
				rot.x = bridgeRot;
				break;
			case axis.Y :
				rot.y = bridgeRot;
				break;
			case axis.Z :
				rot.z = bridgeRot;
				break;
			}
			bridge.localRotation = Quaternion.Euler(rot);
		}
	}
	
	public bool switchLevar()
	{
		if(_timer >= bridgeAnimDuration)
		{
			_timer = 0f;
			bridgeDown = !bridgeDown;
			if(secondLevar != null)
			{
				secondLevar.switchLevar();
			}
			
			SoundManager.askForSoundPlay(transform, leverSound);			
			
			return true;
		}
		return false;
	}
	
}
