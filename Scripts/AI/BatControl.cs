using UnityEngine;
using System.Collections;

public class BatControl : MonoBehaviour {
	
	
	private Vector3 _target;
	private BatsAIControl _global;
	
	private float maxAngle = 120f;
	private float duration = 0.2f;
	private float timer = 0f;

	
	public Transform leftWing;
	public Transform rightWing;
	public Transform leftWingAttach;
	public Transform rightWingAttach;
	
	
	
	// Use this for initialization
	void Start () {
		
		_global = GameObject.Find("BatShape").GetComponent<BatsAIControl>();
		transform.localPosition = Random.insideUnitSphere * 2f;
		newTarget();
	}
	
	// Update is called once per frame
	void Update () {
		
		Vector3 move = _target - transform.localPosition;
		
		if(move.magnitude < _global.radiusReach)
		{
			newTarget();
		}
		else
		{
			move.Normalize();
			move *= _global.speed * Time.deltaTime;
			transform.localPosition += move;
		}
		
		timer += Time.deltaTime;
		
		float angle/* = Mathf.PingPong(Time.time * wingSpeed, maxAngle)*/;
			
		if(timer < duration)
		{
			angle = Mathf.Lerp(-maxAngle/2f, maxAngle/2f, (timer/duration));
		}
		else if(timer < duration * 2f)
		{
			angle = Mathf.Lerp(maxAngle/2f, -maxAngle/2f, ((timer - duration)/duration));
		}
		else
		{
			timer = 0f;
			angle  = -maxAngle/2f;
		}
		
		Transform t = leftWing.transform;
		float deltaAngle = angle - t.localRotation.eulerAngles.z; 
		t.RotateAround(leftWingAttach.position, t.forward, deltaAngle);
		
		
		angle = -angle;
		t = rightWing.transform;
		deltaAngle = angle - t.localRotation.eulerAngles.z; 
		t.RotateAround(rightWingAttach.position, t.forward, deltaAngle);
		
	}
	
	private void newTarget()
	{
		_target = Random.insideUnitSphere;
		_target.x *= _global.xModifier;
		_target.y *= _global.yModifier;
		_target.z *= _global.zModifier;
		_target.y = Mathf.Abs(_target.y * 2f);
		transform.LookAt(transform.TransformPoint(_target));
	}
}
