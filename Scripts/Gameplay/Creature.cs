using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Creature : MonoBehaviour {
	
	
	public Vector3 scale = Vector3.one;
	public float _scaleModifier = 0.3f;
	public float _scaleLossModifier = 0.5f/120f;
	public float _deadThresold = 0.5f;

	
	protected string _name;
	protected Type _type;
	protected Vector3 _eatenScale = Vector3.one;
	
	protected List<Type> _preyedTypes;
	
	public enum Type {
		Player,
		Human,
		Ghost,
		Vampire,
		Ghoul,
		Cleric
	}
	
	public virtual void Awake()
	{
		
	}

	// Use this for initialization
	public virtual void Start () {
		scale = Vector3.one;
	}
	
	// Update is called once per frame
	public virtual void Update () {

		scale.x -= _scaleLossModifier * Time.deltaTime;
		scale.y -= _scaleLossModifier * Time.deltaTime;
		scale.z -= _scaleLossModifier * Time.deltaTime;
		
		transform.localScale = scale;
		
	}
	
	public virtual bool eat(Vector3 creaScale)
	{
		Vector3 scaleModif = _scaleModifier * creaScale;
		scale += scaleModif;
		transform.position = new Vector3(transform.position.x, scale.y * 0.8f, transform.position.z);
		
		return true;
	}


	public Type CreatureType {
		get {
			return this._type;
		}
	}
	
	public bool isPreyingOn(Type typ)
	{
		return _preyedTypes.Contains(typ);	
	}
	
	public virtual bool isDead()
	{
		return	scale.y < _deadThresold || scale.x/scale.y < _deadThresold;
	}
	
	
	public float getSpeedRatio()
	{
		return Mathf.Sqrt(scale.y/scale.x);
	}
	
	public Vector3 getEatenScale()
	{
		return _eatenScale;	
	}
}
