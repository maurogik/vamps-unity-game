using UnityEngine;
using System.Collections;

public class GUIItem {
	
	private string _bkupStyle;
	protected string _style;
	protected string activeStyleExtension = "Active";
	protected bool _selected;
	protected Rect _position;
	
	public GUIItem(string style)
	{
		_style = style;
		_bkupStyle = _style;
		_position = new Rect(0, 0, 0, 0);
	}
	
	public virtual void draw()
	{
	}
	
	public void setSelected(bool selected)
	{
		_selected = selected;
		if(_selected)
		{
			_style = _bkupStyle + activeStyleExtension;	
		}
		else
		{
			_style = _bkupStyle;	
		}
	}
	
	public Rect getRect()
	{
		return _position;	
	}
	
	public virtual void forceValChange(float val)
	{
		
	}
	
	public virtual void forceAction()
	{
		
	}
	
	public bool isSelected()
	{
		return _selected;	
	}
}
