using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// AI ghoul control.
/// </summary>
public class AIGhoulControl : AICreatureControl {


	private TombHandler _nextReachedTomb = null;
	
	
	protected override void collisionOnPath(RaycastHit hit)
	{
		_path = null;
		_nextReachedTomb = null;
		newTarget();	
	}
	
	
	public override void newTarget ()
	{
		if(_path == null || _currentPathTarget >= _path.Count - 1)
		{
			
			if(_path != null && _nextReachedTomb != null && !_nextReachedTomb.enabled
				&& (_nextReachedTomb.transform.position - transform.position).magnitude < actDistance)
			{
				_nextReachedTomb.enabled = true;
				_nextReachedTomb.setOccupant(transform);
			}
			
			if(!_waitingForPath)
			{
				GameObject AI = GameObject.Find("AI");
				MapGenerator map = AI.GetComponent<MapGenerator>();
				List<Transform> tombs = new List<Transform>();
				tombs.AddRange(map.getTombs());
				
				//find a tomb to reach
				int i = Random.Range(0, tombs.Count - 1);
				_nextReachedTomb = tombs[i].GetComponent<TombHandler>();
				
				while(_nextReachedTomb.enabled)
				{
					i = Random.Range(0, tombs.Count - 1);
					_nextReachedTomb = tombs[i].GetComponent<TombHandler>();
				}
				
				Vector3 end = tombs[i].position;
				Vector3 start = transform.position;
				
				_currentPathTarget = 0;
				_path = null;
				Scheduler.aksForPath( start, end, this);
				_waitingForPath = true;
			}
			
			if(_path != null)
			{
				_target = _path[_currentPathTarget];
			}
			else
			{
				base.newTarget();
			}	
			
		}
		else
		{
			
			_currentPathTarget = getNextReachable(_path, _currentPathTarget);
			_target = _path[ _currentPathTarget];
			_target.y = 1f;
			
		}		
	}
	
	
}
