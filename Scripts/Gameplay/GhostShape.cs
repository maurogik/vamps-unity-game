using UnityEngine;
using System.Collections;

public class GhostShape : Power {
	
	
	
	private GameObject _player = null;
	private Material _ghostMatHead;
	private Material _ghostMatBody;
	private Material _normalBodyMat;
	private Material _normalHeadMat;
	private GameObject _head;
	private GameObject _vamp;
	private Vector3 _gravity;
	private float ghostYpos = 1f;
	private Rigidbody _rigidBody;
	
	private float _timerFall = 0f;
	private float _durationFall = 1.5f;
	
	
	public GhostShape(float duration, float cooldown)
		: base(duration, cooldown)
	{
		GameObject _player = GameObject.Find("player");
		_type = Player.PowerType.GHOST_SHAPE;
		_rigidBody = _player.rigidbody;
		_ghostMatHead = _player.GetComponent<Player>().ghostMatHead;
		_ghostMatBody = _player.GetComponent<Player>().ghostMatBody;
		_head = GameObject.Find("head");
		_vamp = GameObject.Find("vamp");
		_normalBodyMat = _vamp.renderer.material;
		_normalHeadMat = _head.renderer.material;
	}
	
	public override bool activate()
	{
		if(base.activate())
		{
			_timerFall = 0f;		
			_vamp.renderer.material = _ghostMatBody;
			_head.renderer.material = _ghostMatHead;
			_rigidBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ |
				RigidbodyConstraints.FreezeRotation;
			return true;
		}
		
		return false;
	}
	
	public override void update (float dt)
	{
		base.update (dt);
		if(_activated)
		{
			if(_player == null)
			{
				_player = GameObject.Find("player");	
			}
			_timerFall += dt;
			Vector3 pos = _player.transform.position;
			float y = pos.y;
			
			y = Mathf.Lerp(y, ghostYpos, _timerFall/_durationFall);
			pos.y = y;
			
			_player.transform.position = pos;
		}
	}
	
	protected override void hide()
	{
		_vamp.renderer.material = _normalBodyMat;
		_head.renderer.material = _normalHeadMat;
		_rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
	}
}
