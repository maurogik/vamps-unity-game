using UnityEngine;
using System.Collections;

/// <summary>
/// AI cleric control.
/// </summary>
public class AIClericControl : AICreatureControl {
	
	/// <summary>
	/// The group leader.
	/// </summary>
	public AIPopeControl leader;
	
	
	private float _lastDistToTarget = 0f;
	private float _stuckDistance = 50f;
	
	
	public override void Start ()
	{
		base.Start ();
		leader.popeDeadEvent += popeDies;
	}
	
	// Update is called once per frame
	public override void Update () {
	
	}
	
	public void LateUpdate()
	{
		if(leader != null)
		{
			//get the target from the leader
			Vector3 tmpTarget = leader.requestTarget();
			
			if(_path == null || _path.Count == 0)
			{
				_target = tmpTarget;
				
				if(_lastDistToTarget > _stuckDistance && !_waitingForPath)
				{

					Vector3 end = leader.transform.position;
					Vector3 start = transform.position;
					
					_currentPathTarget = 0;

					_path = null;
					
					Scheduler.aksForPath( start, end, this);
					_waitingForPath = true;
					
					if(_path != null)
					{
						_target = _path[_currentPathTarget];
					}
					else
					{
						base.newTarget();	
					}
				}
			}
			
			//Does to need to follow a path if the leader target is in sight
			Vector3 direction = tmpTarget - transform.position;
			RaycastHit hit;
			if(!Physics.Raycast(transform.position, direction, out hit, direction.magnitude) 
				|| hit.collider.gameObject.tag != "wall")
			{
				_path = null;
				_currentPathTarget = 0;
				_target = tmpTarget;
			}

			_lastDistToTarget = (tmpTarget - transform.position).magnitude;

		}
		
		base.Update();
	}
	
	protected override void collisionOnPath(RaycastHit hit)
	{
		_path = null;
		_currentPathTarget = 0;
	}
	
	public override void newTarget()
	{
		if(leader != null)
		{
			if(_path != null && _currentPathTarget < _path.Count - 1)
			{
				_currentPathTarget = getNextReachable(_path, _currentPathTarget);
				_target = _path[ _currentPathTarget];
				_target.y = 1f;
			}
			else
			{
				_lastDistToTarget = 0f;
				_currentPathTarget = 0;
				_path = null;
			}
		}
		else
		{
			base.newTarget();	
		}
	}	
	
	protected override void encounter ()
	{
		//if a lost cleric find a new popes, he follows him
		if( _lastCreatureHit.CreatureType == Creature.Type.Cleric)
		{
			AIPopeControl pope = _lastCreatureHit.transform.GetComponent<AIPopeControl>();
			AIClericControl cleric = _lastCreatureHit.transform.GetComponent<AIClericControl>();
			pope = pope != null ? pope : cleric.leader;
			//Follow the new pope
			if(pope != null && leader == null)
			{
				setLeader(pope);
			}
		}
		else if(_lastCreatureHit.CreatureType != Creature.Type.Human )
		{	
			if((_lastCreatureHit.transform.position - transform.position).magnitude < actDistance)
			{
				act();	
			}
		}
	}
	
	/// <summary>
	/// Code to run whenever the leader dies
	/// </summary>
	private void popeDies()
	{
		leader = null;
		EventHandler.killAction -= catchEvent;
		transform.parent = GameObject.Find("LostClerics").transform;
		EventHandler.killAction += catchEvent;
		newTarget();
	}
	
	public void setLeader(AIPopeControl lead)
	{
		leader = lead;
		transform.parent = leader.transform.parent;
	}
	
	public override void kill ()
	{
		if(leader != null)
		{
			leader.popeDeadEvent -= popeDies;
		}
		base.kill ();
	}
}
