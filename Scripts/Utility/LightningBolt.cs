/*
	This script is placed in public domain. The author takes no responsibility for any possible harm.
	Contributed by Jonathan Czeck
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightningBolt : MonoBehaviour
{
	public Transform target;
	public int zigs = 100;
	public float speed = 1f;
	public float scale = 1f;
	public Light startLight;
	public Light endLight;
	public List<LightningBolt> chainedChildren;
	
	Perlin noise;
	float oneOverZigs;
	
	private Particle[] particles;
	
	void Start()
	{
		oneOverZigs = 1f / (float)zigs;
		particleEmitter.emit = false;

		particleEmitter.Emit(zigs);
		particles = particleEmitter.particles;
	}
	
	void Update ()
	{
		if(target != null)
		{
			if (noise == null)
				noise = new Perlin();
				
			float timex = Time.time * speed * 0.1365143f;
			float timey = Time.time * speed * 1.21688f;
			float timez = Time.time * speed * 2.5564f;
			
			for (int i=0; i < particles.Length; i++)
			{
				Vector3 position = Vector3.Lerp(transform.position, target.position, oneOverZigs * (float)i);
				Vector3 offset = new Vector3(noise.Noise(timex + position.x, timex + position.y, timex + position.z),
											noise.Noise(timey + position.x, timey + position.y, timey + position.z),
											noise.Noise(timez + position.x, timez + position.y, timez + position.z));
				position += (offset * scale * ((float)i * oneOverZigs));
				
				particles[i].position = position;
				particles[i].color = Color.white;
				particles[i].energy = 1f;
			}
			
			particleEmitter.particles = particles;
			
			if (particleEmitter.particleCount >= 2)
			{
				if (startLight)
					startLight.transform.position = particles[0].position;
				if (endLight)
					endLight.transform.position = particles[particles.Length - 1].position;
			}
		}
	}
	
	
	public void cleanChainAndDestroy()
	{
		if(chainedChildren != null)
		{
			foreach(LightningBolt bolt in chainedChildren)
			{
				if(bolt != null)
				{
					bolt.cleanChainAndDestroy();
				}
			}
			
			chainedChildren = null;
		}
		if(gameObject != null)
		{
			Destroy(gameObject);
		}
	}

	
	public bool checkBoltValidity(float maxLength)
	{
		if(chainedChildren != null)
		{
			List<int> toRemove = new List<int>();
			int i = 0;
			foreach(LightningBolt bolt in chainedChildren)
			{
				if(bolt != null)
				{
					if(!bolt.checkBoltValidity(maxLength))
					{
						toRemove.Add(i);
					}
				}
				++i;
			}
			
			/*foreach(int rem in toRemove)
			{
				chainedChildren.RemoveAt(rem);	
			}*/
		}
		
		if(target == null || transform == null || (transform.position - target.position).magnitude > maxLength)
		{
			cleanChainAndDestroy();
			return false;
		}
		
		return true;
	}
	
	public Vector3 eatFromChain(float modifier)
	{
		Vector3 gain = Vector3.zero;
		
		if(chainedChildren != null)
		{
			foreach(LightningBolt bolt in chainedChildren)
			{
				gain += bolt.eatFromChain(modifier);
			}
		}
		
		if(target!= null)
		{
			target.GetComponent<Creature>().scale -= Vector3.one * modifier;
			gain += target.GetComponent<Creature>().getEatenScale() * modifier;
		}
		
		return gain;
	}
}