using UnityEngine;
using System.Collections.Generic;

public class Cleric : Creature {


	public override void Awake()
	{
		_type = Type.Cleric;
		_preyedTypes = new List<Type>();
		_preyedTypes.Add(Creature.Type.Ghost);
		_preyedTypes.Add(Creature.Type.Ghoul);
		_preyedTypes.Add(Creature.Type.Player);
		_preyedTypes.Add(Creature.Type.Vampire);
	}
}
