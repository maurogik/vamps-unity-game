using UnityEngine;
using System.Collections;

public class LoadMenu : GUIMenu {
	
	
	
	public delegate string TextGetter();
	
	
	private TextGetter _getter;
	
	
	public LoadMenu(TextGetter getter)
	{
		_getter = getter;	
	}
	
	public override void draw ()
	{
		base.draw ();
		
		float areaWidth = 680f * GUIManager.widthCoef;
		float areaHeight = 800f * GUIManager.heightCoef;
		float areaX = 625f * GUIManager.widthCoef;
		float areaY = 225f * GUIManager.heightCoef;
		
		GUI.Label(new Rect(0f, 0f, Screen.width, Screen.height), "", "loadingBackground");
		GUILayout.BeginArea(new Rect(areaX, areaY, areaWidth, areaHeight));
		GUILayout.BeginVertical();
		
		GUILayout.FlexibleSpace();
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		GUILayout.Label(_getter(), "loadingText");
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.FlexibleSpace();
		
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
	
}
