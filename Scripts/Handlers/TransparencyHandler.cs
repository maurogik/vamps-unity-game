using UnityEngine;
using System.Collections.Generic;

public class TransparencyHandler : MonoBehaviour {
	
	
	public Material regularMat;
	public Material transparentMat;
	public List<Transform> toChange;

	
	public void switchOn()
	{
		if(renderer != null)
		{
			renderer.material = transparentMat;
		}
		
		foreach(Transform trans in toChange)
		{
			trans.renderer.material = transparentMat;	
		}
	}
	
	public void switchOff()
	{
		if(renderer != null)
		{
			renderer.material = regularMat;
		}
		
		foreach(Transform trans in toChange)
		{
			trans.renderer.material = regularMat;	
		}
	}
}
