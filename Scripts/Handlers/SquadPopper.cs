using UnityEngine;
using System.Collections;

public class SquadPopper : MonoBehaviour {
	
	
	public Transform squadPrefab;
	public float openDoorDuration = 15f;
	public LevarHandler levar;
	
	private float _timer = 0f;
	
	
	// Use this for initialization
	void Start ()
	{
	}
	
	void OnEnable()
	{
		_timer = 0f;
		
		Debug.Log("enable popper");
		Transform squad = Instantiate(squadPrefab, transform.position, Quaternion.Euler(-transform.forward)) as Transform;
		squad.parent = GameObject.Find("Squads").transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
		_timer += Time.deltaTime;
		
		if(_timer > openDoorDuration)
		{
			levar.switchLevar();
			enabled = false;
		}
	}
	
	public void attemptToPop()
	{
		if(!enabled && levar.switchLevar())
		{
			enabled = true;
		}
	}
}
