using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Ghost : Creature {

	public override void Awake ()
	{
		_type = Type.Ghost;
		_preyedTypes = new List<Type>(){Type.Human, Type.Ghoul};
		_eatenScale = new Vector3(0.1f, 0.8f, 0.1f);
	}
}
