using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AICreatureControl : MonoBehaviour {
	
	/// <summary>
	/// Should the creature print aditional debug info
	/// </summary>
	public bool debugInfoOn = false;
	
	//raycast variables
	public float rayLength = 5f;
	public float rayDispertion = 0.2f;
	public float rayHeight = 1f;
	public float minCollisionLength = 15f;
	
	//Movement data
	public float actDistance = 3f;
	public float slowDownRadius = 3f;
	public float radiusReach = 1f;
	public float maxSpeed = 10f;
	public float maxPursueDistance = 150f;
	public float bendingCoef;
	public float maxBending = 45f;
	public float rotationDamping = 5f;
	
	//target finding data
	public float xModif = 60f;
	public float yModif = 0f;
	public float zModif = 60f;


	public float eventActivationDistance = 50f;
	
	//time between each base sound playing
	public float baseSoundFrequency = 20f;
	private float _timeSinceLastSound = 0f;
	
	
	//path data
	protected List<Vector3> _path = null;
	protected int _currentPathTarget = -1;
	protected Vector3 _target;
	protected bool _waitingForPath = false;
	
	protected Creature _creature;
	
	//encoutered creatures data
	protected Creature _toFollow = null;
	protected Creature _toFlee = null;
	protected Creature _tmpAvoid = null;
	protected Creature _lastCreatureHit = null;
	protected GameObject _lastObjectHit = null;
	
	//mouvement smoothing
	protected int _nbOfMoves = 20;
	protected List<Vector3> _lastMoves;
	
	//event data
	private bool _listeningForEvent = true;
	private float _lastEventTime;
	private float _eventMaxDuration = 15f;

	
	
	// Use this for initialization
	public virtual void Start () {
		newTarget();
		_creature = GetComponent<Creature>();
		_lastMoves = new List<Vector3>();
		
		for(int i = 0; i < _nbOfMoves; ++i)
		{
			_lastMoves.Add(Vector3.zero);
		}
		
		EventHandler.killAction += catchEvent;
	}
	
	// Update is called once per frame
	public virtual void Update ()
	{
		if(!_listeningForEvent && Time.time - _lastEventTime > _eventMaxDuration)
		{
			_listeningForEvent = true;
			_lastEventTime = 0f;
		}
		
		if(!_creature.isDead())
		{
			runRaycastCheck();
			
			//move the creature
			Vector3 move = getMovement();
			performMovement(move);
		}
		
		if(_toFlee != null)
		{
			GetComponent<CreatureSoundHandler>().playFleeSound();	
		}
		
		if(_toFollow != null)
		{
			GetComponent<CreatureSoundHandler>().playBaseSound();	
		}
		
		if(Time.time - _timeSinceLastSound > baseSoundFrequency * Random.value)
		{
			GetComponent<CreatureSoundHandler>().playBaseSound();
			_timeSinceLastSound = Time.time;
		}
		
		if(_creature.isDead())
		{
			kill ();	
		}
	}
	
	
	void OnDisable()
	{
		_lastMoves = new List<Vector3>();
		
		for(int i = 0; i < _nbOfMoves; ++i)
		{
			_lastMoves.Add(Vector3.zero);
		}
	}
	
	/// <summary>
	/// smooth and performs the movement.
	/// </summary>
	/// <param name='move'>
	/// Move.
	/// </param>
	protected void performMovement(Vector3 move)
	{
		
		Vector3 smoothedMove = Vector3.zero;
		
		_lastMoves.RemoveAt(0);
		_lastMoves.Add(move);
		
		foreach(Vector3 prevMove in _lastMoves)
		{
			smoothedMove += prevMove;	
		}
		
		smoothedMove = new Vector3(smoothedMove.x / _nbOfMoves, smoothedMove.y / _nbOfMoves, smoothedMove.z / _nbOfMoves);
		
		
		smoothedMove = smoothedMove * Time.deltaTime;
		
		Vector3 newPos = smoothedMove + transform.position;

		transform.LookAt(newPos);
		bentFromMovement(smoothedMove/Time.deltaTime, transform);
		transform.position = newPos;
	}
	
	/// <summary>
	/// Runs the raycasts to check if there is an object nearby.
	/// </summary>
	protected void runRaycastCheck()
	{
		//raycasts & collision check
		RaycastHit hit;
		rayHeight = transform.position.y;
		Vector3 forward = Vector3.forward;
		
		Vector3 rayCenter = transform.TransformDirection(forward);
		
		forward = Vector3.forward;
		forward .x -= rayDispertion;
		Vector3 rayLeft = transform.TransformDirection(forward);
		
		forward = Vector3.forward;
		forward.x += rayDispertion;
		Vector3 rayRight = transform.TransformDirection(forward);
		
		forward = Vector3.forward;
		forward .x -= rayDispertion * 2f;
		Vector3 rayLeft2 = transform.TransformDirection(forward);
		
		forward = Vector3.forward;
		forward.x += rayDispertion * 2f;
		Vector3 rayRight2 = transform.TransformDirection(forward);
		
		forward = Vector3.forward;
		forward .x -= rayDispertion * 3f;
		Vector3 rayLeft3 = transform.TransformDirection(forward);
		
		forward = Vector3.forward;
		forward.x += rayDispertion * 3f;
		Vector3 rayRight3 = transform.TransformDirection(forward);
		
		_lastCreatureHit = null;
		_lastObjectHit = null;
		
		Vector3 rayStart = transform.position;
		rayStart.y = rayHeight;
		
		Debug.DrawRay(rayStart, rayCenter * rayLength * _creature.scale.y, Color.red);
		Debug.DrawRay(rayStart, rayLeft * rayLength * _creature.scale.y, Color.red);
		Debug.DrawRay(rayStart, rayRight * rayLength * _creature.scale.y, Color.red);
		Debug.DrawRay(rayStart, rayLeft2 * rayLength * _creature.scale.y, Color.red);
		Debug.DrawRay(rayStart, rayRight2 * rayLength * _creature.scale.y, Color.red);
		Debug.DrawRay(rayStart, rayLeft3 * rayLength * _creature.scale.y, Color.red);
		Debug.DrawRay(rayStart, rayRight3 * rayLength * _creature.scale.y, Color.red);
		
		if(Physics.Raycast(rayStart, rayCenter, out hit, rayLength * _creature.scale.y) 
			|| Physics.Raycast(rayStart, rayLeft, out hit, rayLength * _creature.scale.y)
			|| Physics.Raycast(rayStart, rayRight, out hit, rayLength * _creature.scale.y) 
			|| Physics.Raycast(rayStart, rayLeft2, out hit, rayLength * _creature.scale.y)
			|| Physics.Raycast(rayStart, rayRight2, out hit, rayLength * _creature.scale.y) 
			|| Physics.Raycast(rayStart, rayLeft3, out hit, rayLength * _creature.scale.y)
			|| Physics.Raycast(rayStart, rayRight3, out hit, rayLength * _creature.scale.y))
		{
			GameObject objHit = hit.transform.gameObject;
			Creature creaHit = objHit ? objHit.GetComponent<Creature>() : null;
			
			handleHit(objHit, creaHit);
			
		}	
	}
	
	/// <summary>
	/// Handles the hit on an object.
	/// </summary>
	/// <param name='objectHit'>
	/// Object hit.
	/// </param>
	/// <param name='creatureHit'>
	/// Creature hit.
	/// </param>
	protected virtual void handleHit(GameObject objectHit, Creature creatureHit)
	{
			_lastObjectHit = objectHit;
			_lastCreatureHit = creatureHit;
			_lastObjectHit = _lastCreatureHit ? _lastObjectHit : null;
			if(_lastCreatureHit != null)
			{
				encounter();
			}
	}
	
	/// <summary>
	/// Act on the hit creature.
	/// </summary>
	public virtual bool act()
	{
		LightningHandler boltHandler = GetComponent<LightningHandler>();
		boltHandler.setTarget(_lastCreatureHit);
		
		return false;
	}
	
	/// <summary>
	/// Encounter a creature.
	/// </summary>
	protected virtual void encounter()
	{
		if(_lastCreatureHit.isPreyingOn(_creature.CreatureType))
		{
			_toFlee = _lastCreatureHit;
		}
		else if(_creature.isPreyingOn(_lastCreatureHit.CreatureType))
		{	
			if((_lastCreatureHit.transform.position - transform.position).magnitude < actDistance)
			{
				act();	
			}
			_toFollow = _lastCreatureHit;
		}
		else
		{
			GetComponent<CreatureSoundHandler>().playBaseSound();
			_tmpAvoid = _lastCreatureHit;
		}
	}
	
	
	protected virtual void collisionOnPath(RaycastHit hit)
	{
		newTarget();	
	}
	
	/// <summary>
	/// Bents the transform according to the movement.
	/// </summary>
	/// <param name='move'>
	/// Move.
	/// </param>
	/// <param name='transfo'>
	/// Transfo.
	/// </param>
	protected virtual void bentFromMovement(Vector3 move, Transform transfo)
	{
		if(move.magnitude > 0)
		{
			move = transform.InverseTransformDirection(move);
			Vector3 rot = transform.rotation.eulerAngles;
			rot.y = Mathf.LerpAngle(transfo.eulerAngles.y, rot.y, Time.deltaTime * rotationDamping);
			float xbend = move.z * bendingCoef;
			float zbend = - move.x * bendingCoef;
			rot.z = zbend < 0 ? Mathf.Max(-maxBending, zbend) : Mathf.Min(maxBending, zbend);
			rot.x = xbend < 0 ? Mathf.Max(-maxBending, xbend) : Mathf.Min(maxBending, xbend);
			
			transfo.rotation = Quaternion.Euler(rot);
		}
		else{
			transfo.rotation = Quaternion.Euler(new Vector3(0f, transfo.eulerAngles.y, 0f));
		}
	}
	
	protected virtual Vector3 getMovement()
	{
		Vector3 moveDirection;
		
		//if we are running away
		if(_toFlee != null)
		{
			moveDirection = transform.position - _toFlee.transform.position;

			if(!checkForLevelCollision(transform.position, transform.position + moveDirection))
			{
				if(checkForLevelCollision(transform.position, _toFlee.transform.position)|| moveDirection.magnitude > maxPursueDistance)
				{
					_toFlee = null;	
				}
				else
				{
					Debug.DrawRay(transform.position, moveDirection, Color.red);
				}
			}
			else
			{
				_toFlee = null;
				_listeningForEvent = true;
			}
		}
		//if we are chasing someone
		else if(_toFollow != null)
		{
			moveDirection = _toFollow.transform.position - transform.position;
			if(checkForLevelCollision(transform.position, _toFollow.transform.position) || moveDirection.magnitude > maxPursueDistance)
			{				
				_toFollow = null;
				_listeningForEvent = true;
			}
			else
			{
				Debug.DrawRay(transform.position, moveDirection, Color.green);
			}
		}
		else
		{ // regular target following behavior
					
			moveDirection = (_target - transform.position);
			
			Debug.DrawRay(transform.position, moveDirection, Color.blue);
			
			if(_path != null)
			{
				for(int i = 0; i < _path.Count - 1; ++i)
				{
					Debug.DrawRay(_path[i], _path[i+1] - _path[i], Color.green);
				}	
			}
			
			if(moveDirection.magnitude < radiusReach || _target.y > transform.position.y + 1f)
			{
				newTarget();	
			}
			else if(moveDirection.magnitude > slowDownRadius)
			{
				moveDirection = moveDirection.normalized * maxSpeed;
			}
			
			RaycastHit hit;
			if(checkForLevelCollision(transform.position, _target, out hit))
			{
				collisionOnPath(hit);
			}
		}
		
		if(_tmpAvoid != null)
		{
			Vector3 avoidDir = transform.position - _tmpAvoid.transform.position;
			moveDirection = avoidDir.normalized/Mathf.Max (0.1f,avoidDir.magnitude) + moveDirection.normalized;
			Debug.DrawRay(transform.position, moveDirection, Color.white);
			_tmpAvoid = null;
			
		}

		
		moveDirection.y = 0f;
		if(moveDirection.magnitude > maxSpeed)
		{
			moveDirection = moveDirection.normalized * maxSpeed;
		}
		moveDirection = moveDirection * _creature.getSpeedRatio();
		
		return moveDirection;
	}
	
	
	protected bool checkForLevelCollision(Vector3 start, Vector3 end)
	{
		RaycastHit hit;
		return checkForLevelCollision(start, end, out hit);
	}
	
	protected bool checkForLevelCollision(Vector3 start, Vector3 end, out RaycastHit hit)
	{
		float length;
		Vector3 moveDirection = end - start;
		length = Mathf.Max(minCollisionLength, (moveDirection.normalized * maxSpeed  * Time.deltaTime).magnitude);

		bool stuffHit = Physics.Raycast(start, moveDirection, out hit, length);

		stuffHit = stuffHit && (hit.collider.gameObject.tag == "wall" 
			|| hit.collider.gameObject.tag == "externWall");
		
		return stuffHit;
	}
	
	/// <summary>
	/// Find a new reachable target
	/// </summary>
	public virtual void newTarget()
	{
		_target = Random.insideUnitSphere;
		_target.x *= xModif;
		_target.y = 0f;
		_target.z *= zModif;
		_target += transform.position;
		
		Vector3 move = _target - transform.position;
		while(Physics.Raycast(transform.position, move, move.magnitude))
		{
			_target = Random.insideUnitSphere;
			_target.x *= xModif;
			_target.y = 0f;
			_target.z *= zModif;
			_target += transform.position;
			move = _target - transform.position;
		}
	}
	
	/// <summary>
	/// Gets the next reachable node on the path.
	/// </summary>
	/// <returns>
	/// The next reachable.
	/// </returns>
	/// <param name='path'>
	/// Path.
	/// </param>
	/// <param name='startInd'>
	/// Start ind.
	/// </param>
	protected int getNextReachable(List<Vector3> path, int startInd)
	{
		Vector3 pos = transform.position;
		int res =  1 + startInd;
		for(int i = res; i < path.Count; ++i)
		{
			Vector3 target = path[i];
			if(!Physics.Raycast(pos, (target - pos), (target - pos).magnitude))
			{
				res = i;
			}
		}
		return res;
	}
	
	public virtual void kill()
	{
		//remove audio source from current sound source (if it was one)
		SoundManager.removeSource(audio);

		if(_waitingForPath)
		{
			Debug.Log("kill & path over");
			Scheduler.pathFindingOver();
		}
		EventHandler.killAction -= catchEvent;;

		LightningBolt bolt = GetComponentInChildren<LightningBolt>();
		if(bolt != null)
		{
			bolt.cleanChainAndDestroy();	
		}
		GetComponent<CreatureSoundHandler>().playDeathSound(true);
		Destroy(gameObject);	
	}
	
	protected virtual void catchEvent(Creature crea)
	{
		if(crea != null && crea.transform != null && _listeningForEvent && transform != null)
		{
			Vector3 direction = transform.position - crea.transform.position;

			if(direction.magnitude < eventActivationDistance)
			{
				if(_creature.isPreyingOn(crea.CreatureType))
				{
					_toFollow = crea;
				}
				else if(crea.isPreyingOn( _creature.CreatureType))
				{
					_toFlee = crea;
				}
				else
				{
					GetComponent<CreatureSoundHandler>().playBaseSound();
				}
				_listeningForEvent = false;
				_lastEventTime = Time.time;
			}
		}
	}
	
	/// <summary>
	/// Sets this creature path.
	/// </summary>
	/// <param name='path'>
	/// Path.
	/// </param>
	public void setPath(List<Vector3> path)
	{
		if(path != null)
		{
			_waitingForPath = false;
			_path = path;
			_currentPathTarget = 0;
			newTarget();
		}
	}
	
}
