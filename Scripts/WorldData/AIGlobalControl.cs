using UnityEngine;
using System.Collections;

public class AIGlobalControl : LoadingScript {
	
	public int nbHumanMax = 5;
	public int nbVampireMax = 3;
	public int nbGhoulMax = 4;
	public int nbGhostMax = 2;
	public int nbSquadMax = 8;
	
	
	public Transform humanPrefab;
	public Transform vampirePrefab;
	public Transform ghoulPrefab;
	public Transform ghostPrefab;
	public Transform squadPrefab;
	
	public float popDispertion = 100f;
	
	private float _startPosY = 0.8f;
	private SquadPopper _squadPopper;
	
	public void pauseAllAI(bool enabled)
	{
		foreach(AICreatureControl control in transform.GetComponentsInChildren<AICreatureControl>())
		{
			control.enabled = enabled;	
		}
	}
	
	
	
	public bool AIGui()
	{
		bool quitGame = false;
		int nbHuman, nbGhost, nbGhoul, nbVamps, nbSquad;
		nbHuman = transform.FindChild("Humans").transform.childCount;
		nbGhost = transform.FindChild("Ghosts").transform.childCount;
		nbGhoul = transform.FindChild("Ghouls").transform.childCount;
		nbVamps = transform.FindChild("Vampires").transform.childCount;
		nbSquad = transform.FindChild("Squads").transform.childCount;
		
		GUILayout.BeginArea(new Rect(50f, 50f, 600f, 200f));
		GUILayout.BeginVertical();
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Number of human : " + nbHuman);
		nbHumanMax = (int)GUILayout.HorizontalSlider(nbHumanMax,3, 200);
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Number of ghouls : " + nbGhoul);
		nbGhoulMax = (int)GUILayout.HorizontalSlider(nbGhoulMax,3, 100);
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Number of vamps : " + nbVamps);
		nbVampireMax = (int)GUILayout.HorizontalSlider(nbVampireMax,3, 100);
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Number of ghosts : " + nbGhost);
		nbGhostMax = (int)GUILayout.HorizontalSlider(nbGhostMax,3, 100);
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Number of squads : " + nbSquad);
		nbSquadMax = (int)GUILayout.HorizontalSlider(nbSquadMax,1, 15);
		GUILayout.EndHorizontal();
		
		quitGame = GUILayout.Button("Return to menu", "button");
		
		GUILayout.EndVertical();
		GUILayout.EndArea();
		
		return quitGame;
				
	}
			
	// Use this for initialization
	void Start () {
		_squadPopper = GameObject.Find("ChurchPrefab(Clone)").GetComponent<SquadPopper>();
	}
	
	// Update is called once per frame
	void Update () {
		
		int nbHuman, nbGhost, nbGhoul, nbVamps, nbSquad;
		nbHuman = transform.FindChild("Humans").transform.childCount;
		nbGhost = transform.FindChild("Ghosts").transform.childCount;
		nbGhoul = transform.FindChild("Ghouls").transform.childCount;
		nbVamps = transform.FindChild("Vampires").transform.childCount;
		nbSquad = transform.FindChild("Squads").transform.childCount;
		
		//instatiate all prefabs
		
		for(int i = nbHuman; i < nbHumanMax; ++i)
		{
			Vector3 position = Random.insideUnitSphere * popDispertion;
			position.y = _startPosY;
			Transform newO = Instantiate(humanPrefab, position, Quaternion.Euler(Vector3.zero)) as Transform;
			newO.parent = transform.Find("Humans");
		}
		
		for(int i = nbVamps; i < nbVampireMax; ++i)
		{
			Vector3 position = Random.insideUnitSphere * popDispertion;
			position.y = _startPosY;
			Transform newO = Instantiate(vampirePrefab, position, Quaternion.Euler(Vector3.zero)) as Transform;
			newO.parent = transform.Find("Vampires");
		}
				
		for(int i = nbGhost; i < nbGhostMax; ++i)
		{
			Vector3 position = Random.insideUnitSphere * popDispertion;
			position.y = _startPosY;
			Transform newO = Instantiate(ghostPrefab, position, Quaternion.Euler(Vector3.zero)) as Transform;
			newO.parent = transform.Find("Ghosts");
		}
						
		for(int i = nbGhoul; i < nbGhoulMax; ++i)
		{
			Vector3 position = Random.insideUnitSphere * popDispertion;
			position.y = _startPosY;
			Transform newO = Instantiate(ghoulPrefab, position, Quaternion.Euler(Vector3.zero)) as Transform;
			newO.parent = transform.Find("Ghouls");
		}
		
		for(int i = nbSquad; i < nbSquadMax; ++i)
		{
			/*Vector3 position = Random.insideUnitSphere * popDispertion;
			position.y = _startPosY;
			Transform newO = Instantiate(squadPrefab, position, Quaternion.Euler(Vector3.zero)) as Transform;
			newO.parent = transform.Find("Squads");*/
			_squadPopper.attemptToPop();
		}
		
		if(!loadginDone)
		{
			WorldLoader.loaderText = "AI characters generated";
		}
		loadginDone = true;
		
	}
	
	
	public void resetAllAI()
	{
		EventHandler.cleanListeners();
		destroyChildrenOn(transform.Find("Ghosts"));
		destroyChildrenOn(transform.Find("Ghouls"));
		destroyChildrenOn(transform.Find("Vampires"));
		destroyChildrenOn(transform.Find("Humans"));
		destroyChildrenOn(transform.Find("Squads"));
		destroyChildrenOn(transform.Find("LostClerics"));
	}
	
	private void destroyChildrenOn(Transform parent)
	{
		foreach(Transform aiCreature in parent)
		{
			AICreatureControl control = aiCreature.GetComponent<AICreatureControl>();
			if(control != null)
			{
				Debug.Log("killing : "+control.gameObject.name);
				control.kill();
			}
			else
			{
				Debug.Log ("brute destroy of :"+aiCreature.name);
				
				destroyChildrenOn(aiCreature);
				
				Destroy(aiCreature.gameObject);
			}
		}	
	}
	
	
	public float getNbHumanMax()
	{
		return nbHumanMax;	
	}
	
	public float getNbGhostMax()
	{
		return nbGhostMax;	
	}
	
	public float getNbGhoulMax()
	{
		return nbGhoulMax;	
	}
	
	public float getNbVampireMax()
	{
		return nbVampireMax;	
	}
	
	public float getNbSquadMax()
	{
		return nbSquadMax;	
	}
	
	public void setNbHumanMax(int val)
	{
		nbHumanMax = val;	
	}
	
	public void setNbGhostMax(int val)
	{
		nbGhostMax = val;	
	}
	
	public void setNbGhoulMax(int val)
	{
		nbGhoulMax = val;	
	}
	
	public void setNbVampireMax(int val)
	{
		nbVampireMax = val;	
	}
	
	public void setNbSquadMax(int val)
	{
		nbSquadMax = val;	
	}
}
