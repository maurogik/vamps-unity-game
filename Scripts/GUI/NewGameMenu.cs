using UnityEngine;
using System.Collections;

public class NewGameMenu : GUIMenu {

	
	public NewGameMenu()
		: base ()
	{
		initMenu();
	}
	
	private void initMenu()
	{
		float labelHeight = 60f;
		float labelWidth = 300f;
		
		float sliderWidth = 350f;
		float sliderHeight = 60f;
		
		AIGlobalControl aiGlobal = GameObject.Find("AI").GetComponent<AIGlobalControl>();
		

		_items.Add(new GUISlider("Number of humans : ", 0, 80, sliderWidth, sliderHeight,
			labelWidth, labelHeight, new GUISlider.GetterType(aiGlobal.getNbHumanMax),
			new GUISlider.SetterType(aiGlobal.setNbHumanMax)));

		
		_items.Add(new GUISlider("Number of ghouls : ", 0, 20, sliderWidth, sliderHeight,
			labelWidth, labelHeight, new GUISlider.GetterType(aiGlobal.getNbGhoulMax),
			new GUISlider.SetterType(aiGlobal.setNbGhoulMax)));

		
		_items.Add(new GUISlider("Number of vamps : ", 0, 20, sliderWidth, sliderHeight,
			labelWidth, labelHeight, new GUISlider.GetterType(aiGlobal.getNbVampireMax),
			new GUISlider.SetterType(aiGlobal.setNbVampireMax)));
		
		
		_items.Add(new GUISlider("Number of ghosts : ", 0, 20, sliderWidth, sliderHeight,
			labelWidth, labelHeight, new GUISlider.GetterType(aiGlobal.getNbGhostMax),
			new GUISlider.SetterType(aiGlobal.setNbGhostMax)));
		
		
		_items.Add(new GUISlider("Number of squads : ", 0, 10, sliderWidth, sliderHeight,
			labelWidth, labelHeight, new GUISlider.GetterType(aiGlobal.getNbSquadMax),
			new GUISlider.SetterType(aiGlobal.setNbSquadMax)));
		
		
		MapGenerator mapGen = GameObject.Find("AI").GetComponent<MapGenerator>();
	
		
		_items.Add(new GUISlider("Number of houses : ", 5, 100, sliderWidth, sliderHeight,
			labelWidth, labelHeight, new GUISlider.GetterType(mapGen.getNbHouses),
			new GUISlider.SetterType(mapGen.setNbHouses)));
		

		_items.Add(new GUISlider("Number of tombs : ", 5, 50, sliderWidth, sliderHeight,
			labelWidth, labelHeight, new GUISlider.GetterType(mapGen.getNbTombs),
			new GUISlider.SetterType(mapGen.setNbTombs)));
		
		_items.Add(new GUISlider("Number of coffins : ", 5, 50, sliderWidth, sliderHeight,
			labelWidth, labelHeight, new GUISlider.GetterType(mapGen.getNbCoffins),
			new GUISlider.SetterType(mapGen.setNbCoffins)));
		
		
		float buttonHeight = 100f;
		float buttonWidth = 680f;
		GUIManager gui = GameObject.Find("Core").GetComponent<GUIManager>();
		
		_items.Add(new GUIButton("Start game", buttonWidth, buttonHeight, new GUIButton.SetterBool(gui.setLoadGame)));
		
		_items.Add(new GUIButton("Cancel", buttonWidth, buttonHeight, new GUIButton.SetterBool(gui.setMainMenu)));
	}
	
	public override void draw ()
	{
		base.draw ();
		
		float areaWidth = 680f * GUIManager.widthCoef;
		float areaHeight = 800f * GUIManager.heightCoef;
		float areaX = 625f * GUIManager.widthCoef;
		float areaY = 225f * GUIManager.heightCoef;
		
		GUI.Label(new Rect(0f, 0f, Screen.width, Screen.height), "", "loadingBackground");
		GUILayout.BeginArea(new Rect(areaX, areaY, areaWidth, areaHeight));
		
		_scroolPos = GUILayout.BeginScrollView(_scroolPos, "scrollView", "scrollView");
		GUILayout.BeginVertical();
		
		GUILayout.FlexibleSpace();
		
		foreach(GUIItem itm in _items)
		{
			itm.draw();
			//GUILayout.FlexibleSpace();
		}
		
		GUILayout.FlexibleSpace();
		
		GUILayout.EndVertical();
		GUILayout.EndScrollView();
		
		GUILayout.EndArea();
	}
	
	

}
