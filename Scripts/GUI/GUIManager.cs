using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {
	
	public WorldController worldControl;
	public GUISkin skin;
	public static GUISkin s_skin;
	
	public static float widthCoef;
	public static float heightCoef;
	
	private GUIMenu newGame;
	private GUIMenu loadMenu;
	private GUIMenu deathMenu;
	private GUIMenu pauseMenu;
	private GUIMenu mainMenu;
	private GUIMenu creditsMenu;
	private GUIMenu helpMenu;
	
	private GUIMenu _currentMenu;
	
	// Use this for initialization
	void Awake () {
		
		widthCoef = Screen.width / 1920f;
		heightCoef = Screen.height / 1080f;
		newGame = new NewGameMenu();
		loadMenu = new LoadMenu(new LoadMenu.TextGetter(GetComponent<WorldLoader>().getLoaderText));
		deathMenu = new DeathMenu();
		pauseMenu = new PauseMenu();
		mainMenu = new MainMenu();
		
		creditsMenu = new TextMenu("A game by Gwenn AUBERT \n"+
			 "\n Musics : www.newsground.com \n" +
			 "\n Malefic World 2 : Lorens4444 \n" +
			 "March Of The Living : XxX-WOLF-XxX \n" +
			 "A Life Worth Living : carl565 \n" +
			 "Omen : yani76 \n" +
			 "Night Ballad : homelessandhungry \n" +
			 "Ballad of the Vampire : Memphiston \n" +
			"\n SoudEffects : opengameart.org \n" +
			"\n HaelDB\n pal.zoltan.illes\n MaximB\n" +
			"n3b\n kddekadenz\n tinsin\n qubodup\n" +
			"Michel Baradari\n A New Room\n VennStone");
		
		helpMenu = new TextMenu("Controls :\n" +
			"\n Movement : 'Z','Q','S','D' / joystick left \n" +
			"Camera : arrow keys / mouse / joystick right \n" +
			"Attack : return / mouse left / right trigger \n" +
			"Catch : left ctrl / mouse right / left trigger \n" +
			"Jump : space / [A] \n" +
			"Bat Dash : left shift / left stick pressed \n" +
			"Third Eye : 'E' / [Y] \n" +
			"Ghost Shape : 'G' / [B]");
		
		_currentMenu = mainMenu;
		
		s_skin = skin;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		widthCoef = Screen.width / 1920f;
		heightCoef = Screen.height / 1080f;
		
		if(worldControl.GameRunning)
		{
			_currentMenu = null;	
		}
		
		if(_currentMenu != null)
		{
			_currentMenu.update(Time.deltaTime);	
		}
		
		Screen.lockCursor = _currentMenu == null || _currentMenu == loadMenu;

		if(InputManager.input.getButtonDown("Option") && worldControl.GameStarted && !worldControl.isPlayerDead())
		{
			if(worldControl.GameRunning)
			{
				worldControl.pauseGame(true);
				_currentMenu = pauseMenu;
			}
			else
			{
				worldControl.pauseGame(false);
				_currentMenu = null;
			}
		}	
	}
	
	
	void OnGUI()
	{
		GUISkin bkskin = GUI.skin;
		GUI.skin = skin;
		
		if(_currentMenu != null)
		{
			_currentMenu.draw();	
		}
		
		GUI.skin = bkskin;
	}
	
	public void setMainMenu(bool b)
	{
		if(b)
		{
			_currentMenu = mainMenu;
		}
	}
	
	
	public void setNewGame(bool b)
	{
		if(b)
		{
			_currentMenu = newGame;	
		}
	}
	
	public void setLoadGame(bool b)
	{
		if(b)
		{
			_currentMenu = loadMenu;
			GetComponent<WorldLoader>().enabled = true;
		}
	}
	
	public void setQuitGame(bool b)
	{
		if(b)
		{
			Application.Quit();
		}
	}
	
	public void setReturnToMenu(bool b)
	{
		if(b)
		{
			worldControl.resetGame();
			Application.LoadLevel("loading");	
		}
	}
	
	
	public void setDisplayDeathMenu(bool b)
	{
		_currentMenu = deathMenu;	
	}
	
	public void setRetryGame(bool b)
	{
		if(b)
		{
			worldControl.resetGame();
			StartCoroutine(worldControl.startGame());
			_currentMenu = null;
		}		
	}
	
	public void setCreditsMenu(bool b)
	{
		if(b)
		{
			_currentMenu = creditsMenu;
		}
	}
	
	public void setHelpMenu(bool b)
	{
		if(b)
		{
			_currentMenu = helpMenu;
		}
	}

}
