using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class NavigationMap : LoadingScript {
	
	public class Link
	{
		public Vector3 nodeTo;
		public Vector3 nodeFrom;
		public float weight;
	}
	
	class NodeRecord
	{
		public Vector3 node;
		public Link link;
		public float costSoFar;
		public float estimatedTotalCostal;
	
	}
	
	public bool reduceGraphOnStart = true;
	public float graphReductionThreshold = 1.5f;
	public bool drawGraph = false;
	
	public float _baseNodeDistance = 150f;
	private float _baseCoef = 200f;
	private float _inclusionRadius = 200f;
	private List<Link> _graph;

	
	
	void Awake()
	{
		Debug.Log("awakning nav map");
		
	}
	

	
	
	// Use this for initialization
	void Start ()
	{
		Debug.Log("starting navMap");
		StartCoroutine(initializeGraph());
	}

	
	private IEnumerator generateGraphFromLevel()
	{
		//create node list from houses
		
		_graph = new List<Link>();
		
		List<Vector3> nodes = new List<Vector3>();
		List<Transform> blocks = GetComponent<MapGenerator>().getBlocks();
		
		foreach(Transform block in blocks)
		{
			foreach(Transform blockChild in block)
			{
				if(blockChild.tag == "node")
				{
					nodes.Add(blockChild.position);
				}
				/*else if(blockChild.name == "DoorWall")
				{
					blockChild.FindChild("Door").gameObject.SetActive(false);
				}*/
			}

			WorldLoader.loaderText = nodes.Count+" nodes generated";
			yield return null;
		}
		
		//add a link to the center of the map for each node
		Vector3 center = Vector3.zero;
		center.y = 1f;
		
		foreach(Vector3 nod in nodes)
		{
			if(!Physics.Raycast(center, nod - center, (nod - center).magnitude))
			{
				Link link = new Link();
				link.nodeFrom = nod;
				link.nodeTo = center;
				link.weight = (nod - center).magnitude;
				
				_graph.Add(link);
				
				Link linkReturn = new Link();
				linkReturn.nodeFrom = center;
				linkReturn.nodeTo = nod;
				linkReturn.weight = (nod - center).magnitude;
				
				_graph.Add(linkReturn);
			}
		}
		
		
		float maxNodeDistance;
		//compute max distance
		maxNodeDistance = _baseNodeDistance * (_baseCoef/nodes.Count);
		Debug.Log("nb node : "+ nodes.Count + " maxDist : "+maxNodeDistance);
		
		int nbDiscardedNodes = 0;
		foreach(Vector3 nodA in nodes)
		{
			foreach(Vector3 nodB in nodes)
			{
				if(	nodA != nodB )
				{
					if(!Physics.Raycast(nodA, nodB - nodA, (nodB - nodA).magnitude) 
						&& (nodB - nodA).magnitude < maxNodeDistance)
					{
						Link link = new Link();
						link.nodeFrom = nodA;
						link.nodeTo = nodB;
						link.weight = (nodB - nodA).magnitude;
						
						/*List<Vector3> path = getPathFromPos(link.nodeFrom, link.nodeTo);
						if(path == null || path.Count <= 0 || computePathCost(path) > link.weight * 1.5f)
						{*/
							_graph.Add(link);
						/*}
						else
						{
							++nbDiscardedNodes;
						}*/
					}
				}
			}
			
			WorldLoader.loaderText = _graph.Count+" connections generated, " + nbDiscardedNodes +" connections discarded";
			yield return null;
		}
		
	}
	
	private IEnumerator initializeGraph()
	{
		yield return StartCoroutine(generateGraphFromLevel());
		Debug.Log("graph generated : "+_graph.Count);
		if(reduceGraphOnStart)
		{
			yield return StartCoroutine(reduceGraph());
		}
		Debug.Log("graph initialized !");
		loadginDone = true;
	}
	
	private IEnumerator reduceGraph()
	{
		Debug.Log("graph " + _graph.Count);
		Debug.Log("Reducing graph");
		
		int graphCount = _graph.Count;
		
		int i = 0;
		int nbDiscardedNodes = 0;
		
		
		
		while(i < _graph.Count)
		{
			Link link = _graph[i];
			_graph.RemoveAt(i);
			List<Vector3> path = getPathNoYield(link.nodeFrom, link.nodeTo);
			if(path == null || path.Count <= 0 || computePathCost(path) > link.weight * graphReductionThreshold)
			{
				_graph.Insert(i, link);
				++i;
			}
			else
			{
				++nbDiscardedNodes;	
			}
			
			if(i % 10 == 0)
			{
				WorldLoader.loaderText = "Reducing graph : \n"+i+" connection kept\n" + nbDiscardedNodes +" connection discarded "
					+ "\n total of " + (i + nbDiscardedNodes) +" connections processed out of " + graphCount;
				yield return null;	
			}
		}
		
		WorldLoader.loaderText = "Graph reduced from "+graphCount+" to "+_graph.Count;
		yield return new WaitForSeconds(1);
		
		
		Debug.Log("graph " + _graph.Count);

	}
	
	private float computePathCost(List<Vector3> path)
	{
		float cost = 0f;
		
		for(int i = 0; i < path.Count - 1; ++i)
		{
			cost += (path[i+1] - path[i]).magnitude;
		}
		
		return cost;
	}
	
	
	
	// Update is called once per frame
	void Update ()
	{
		if(drawGraph)
		{
			//drawGraph = false;
			StartCoroutine(drawLinks());
		}
	}
	
	
	private IEnumerator drawLinks()
	{
		int i = 0;
		foreach(Link l in _graph)
		{
			Debug.DrawRay(l.nodeFrom, l.nodeTo - l.nodeFrom, Color.white);
			++i;
			if(i % 50 == 0)
			{
				yield return null;	
			}
		}
	}
	
	public Vector3 getClosestNode(Vector3 pos, float inclusionRadius)
	{
		Vector3 closest = Vector3.zero;

		foreach(Link l in _graph)
		{
			Vector3 nod = l.nodeFrom;
			
			if(closest == Vector3.zero || ((closest - pos).magnitude > (nod - pos).magnitude 
				&& !Physics.Raycast(pos, nod - pos, (nod - pos).magnitude)))
			{
				closest = nod;
			}
			
			nod = l.nodeTo;
			
			if(closest == Vector3.zero || ((closest - pos).magnitude > (nod - pos).magnitude 
				&& !Physics.Raycast(pos, nod - pos, (nod - pos).magnitude)))
			{
				closest = nod;
			}
		}
		
		if((closest - pos).magnitude > inclusionRadius)
		{
			closest = Vector3.zero;	
		}
		
		return closest;
	}

	
	private NodeRecord getSmallest(Dictionary<Vector3, NodeRecord> list)
	{
		NodeRecord smallest = new NodeRecord();
		smallest.estimatedTotalCostal = 120000f;
		foreach(NodeRecord rec in list.Values)
		{
			if(rec.estimatedTotalCostal < smallest.estimatedTotalCostal)
			{
				smallest = rec;
			}
		}
		
		return smallest;
	}
	
	private bool containsNode(List<NodeRecord> list, Vector3 node)
	{
		foreach(NodeRecord nodRec in list)
		{
			if(nodRec.node == node)
			{
				return true;	
			}
		}
		return false;
	}
	
	private NodeRecord findNodeRecord(List<NodeRecord> list, Vector3 node)
	{
		foreach(NodeRecord nodRec in list)
		{
			if(nodRec.node == node)
			{
				return nodRec;	
			}
		}
		return null;
	}
	
	private List<Link> getConnections(Vector3 pos)
	{
		List<Link> links = new List<Link>();
		
		foreach(Link l in _graph)
		{
			if(l.nodeFrom == pos)
			{
				links.Add(l);	
			}
		}
		
		return links;
	}
	
	
	public List<Vector3> getPathNoYield(Vector3 startPos, Vector3 goal)
	{
		
		goal = getClosestNode(goal, _inclusionRadius);

		NodeRecord startRecord = new NodeRecord();
		Vector3 start = getClosestNode(startPos, _inclusionRadius);
		
		if(goal == Vector3.zero || start == Vector3.zero)
		{
			return null;	
		}
		startRecord.node = start;
		startRecord.link = null;
		startRecord.costSoFar = 0f;
		startRecord.estimatedTotalCostal = (goal - start).magnitude;
		
		Debug.DrawRay(start, goal - start, Color.green);
		
		Dictionary<Vector3, NodeRecord> openList = new Dictionary<Vector3, NodeRecord>();
		openList[start] = startRecord;
		
		Dictionary<Vector3, NodeRecord> closedList = new Dictionary<Vector3, NodeRecord>();
		
		NodeRecord current = null;
		
		while(openList.Values.Count > 0)
		{
			float endNodeHeuristic;
			NodeRecord endNodeRecord;
			float endNodeCost;
			
			current = getSmallest(openList);
			
			if(current.node == goal)
			{
				break;
			}
			
			foreach(Link link in getConnections(current.node))
			{
				Vector3 endNode = link.nodeTo;
				endNodeCost = current.costSoFar + link.weight;
				
				Debug.DrawRay(link.nodeFrom, (link.nodeTo - link.nodeFrom), Color.red);
				
				if( closedList.ContainsKey(endNode))
				{
					endNodeRecord = closedList[endNode];
					
					if(endNodeRecord.costSoFar <= endNodeCost)
					{
						continue;	
					}
					
					closedList.Remove(endNode);
					
					endNodeHeuristic = endNodeRecord.estimatedTotalCostal - endNodeRecord.costSoFar;
				}
				else if( openList.ContainsKey(endNode))
				{
					endNodeRecord = openList[endNode];
					
					if(endNodeRecord.costSoFar <= endNodeCost)
					{
						continue;	
					}
					endNodeHeuristic = endNodeRecord.estimatedTotalCostal - endNodeRecord.costSoFar;
				}
				else
				{
					endNodeRecord = new NodeRecord();
					endNodeRecord.node = endNode;
					endNodeHeuristic = (goal - endNode).magnitude;
				}
				
				endNodeRecord.costSoFar = endNodeCost;
				endNodeRecord.link = link;
				endNodeRecord.estimatedTotalCostal = endNodeCost + endNodeHeuristic;
				
				if(!openList.ContainsKey(endNode))
				{
					openList[endNode] = endNodeRecord;	
				}
			}
			
			openList.Remove(current.node);
			closedList[current.node] = current;
		}
		
		if(current.node != goal)
		{
			return null;
		}
		else
		{
			List<Vector3> path = retrievePath(current, start, closedList);
			
			return path;
		}
	}
	
	public IEnumerator getPathFromPos(Vector3 startPos, Vector3 goal
		, AICreatureControl requester)
	{
		yield return null;
		//Debug.Log("entering routine");
		goal = getClosestNode(goal, _inclusionRadius);

		NodeRecord startRecord = new NodeRecord();
		Vector3 start = getClosestNode(startPos, _inclusionRadius);
		
		startRecord.node = start;
		startRecord.link = null;
		startRecord.costSoFar = 0f;
		startRecord.estimatedTotalCostal = (goal - start).magnitude;
		
		Debug.DrawRay(start, goal - start, Color.green);
		
		Dictionary<Vector3, NodeRecord> openList = new Dictionary<Vector3, NodeRecord>();
		openList[start] = startRecord;
		
		Dictionary<Vector3, NodeRecord> closedList = new Dictionary<Vector3, NodeRecord>();
		
		NodeRecord current = null;
		
		if(goal == Vector3.zero || start == Vector3.zero)
		{
			requester.setPath(null);
			openList = null;
			closedList = null;
			Scheduler.pathFindingOver();
			yield break;
		}
		
		float startTime = 0f;
		
		while(openList != null && openList.Values.Count > 0)
		{
			
			float endNodeHeuristic;
			NodeRecord endNodeRecord;
			float endNodeCost;
			
			current = getSmallest(openList);
			
			if(current.node == goal)
			{
				break;
			}
			
			if(startTime == 0f)
			{
				startTime = Time.realtimeSinceStartup;	
			}
			//Debug.Log("start time : "+startTime + " current time "  + Time.realtimeSinceStartup);
			if(Time.realtimeSinceStartup - startTime > Scheduler.AvailableTimePerPathfinder
				&& closedList.Count > 0)
			{
				//Debug.Log("stopping pathfinding");
				List<Vector3> path = retrievePath(current, start, closedList);
				requester.setPath(path);
				startTime = 0f;
				//wait until next frame
				yield return null;
			}
			
			foreach(Link link in getConnections(current.node))
			{
				Vector3 endNode = link.nodeTo;
				endNodeCost = current.costSoFar + link.weight;
				
				Debug.DrawRay(link.nodeFrom, (link.nodeTo - link.nodeFrom), Color.red);
				
				if( closedList.ContainsKey(endNode))
				{
					endNodeRecord = closedList[endNode];
					
					if(endNodeRecord.costSoFar <= endNodeCost)
					{
						continue;	
					}
					
					closedList.Remove(endNode);
					
					endNodeHeuristic = endNodeRecord.estimatedTotalCostal - endNodeRecord.costSoFar;
				}
				else if( openList.ContainsKey(endNode))
				{
					endNodeRecord = openList[endNode];
					
					if(endNodeRecord.costSoFar <= endNodeCost)
					{
						continue;	
					}
					endNodeHeuristic = endNodeRecord.estimatedTotalCostal - endNodeRecord.costSoFar;
				}
				else
				{
					endNodeRecord = new NodeRecord();
					endNodeRecord.node = endNode;
					endNodeHeuristic = (goal - endNode).magnitude;
				}
				
				endNodeRecord.costSoFar = endNodeCost;
				endNodeRecord.link = link;
				endNodeRecord.estimatedTotalCostal = endNodeCost + endNodeHeuristic;
				
				if(!openList.ContainsKey(endNode))
				{
					openList[endNode] = endNodeRecord;	
				}
			}
			
			openList.Remove(current.node);
			closedList[current.node] = current;
		}
		
		if( current == null || current.node != goal)
		{
			requester.setPath(null);
		}
		else
		{
			List<Vector3> path = retrievePath(current, start, closedList);
			requester.setPath(path);
		}
		
		Scheduler.pathFindingOver();
	}

	
	private List<Vector3> retrievePath(NodeRecord record, Vector3 startPos
		, Dictionary<Vector3, NodeRecord> closedList)
	{
		
		List<Vector3> path = new List<Vector3>();
		
		/*if(closedList == null || record == null)
		{
			return null;	
		}*/
	
		//Debug.Log("retrieving path");
		while(record.node != startPos && record.link != null)
		{
			path.Add(record.node);
			record = closedList[record.link.nodeFrom] ;
		}
		
		path.Add(startPos);
		
		path.Reverse();
		
		return path;
	}

}
