using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// AI vampire control.
/// </summary>
public class AIVampireControl : AICreatureControl {

	private CoffinHandler _nextReachedCoffin = null;
	
	
	protected override void collisionOnPath(RaycastHit hit)
	{
		_path = null;
		_nextReachedCoffin = null;
		newTarget();	
	}
	
	
	public override void newTarget ()
	{
		if(_path == null || _currentPathTarget >= _path.Count - 1)
		{
			
			if(_path != null && _nextReachedCoffin != null && !_nextReachedCoffin.enabled
				&& (_nextReachedCoffin.transform.position - transform.position).magnitude < actDistance)
			{
				_nextReachedCoffin.enabled = true;
				_nextReachedCoffin.setOccupant(transform);
			}
			
			if(!_waitingForPath)
			{
				GameObject AI = GameObject.Find("AI");
				MapGenerator map = AI.GetComponent<MapGenerator>();
				List<Transform> coffins = new List<Transform>();
				coffins.AddRange(map.getCoffins());
				
				//find a coffin to reach
				int i = Random.Range(0, coffins.Count - 1);
				_nextReachedCoffin = coffins[i].GetComponent<CoffinHandler>();
				
				while(_nextReachedCoffin.enabled)
				{
					i = Random.Range(0, coffins.Count - 1);
					_nextReachedCoffin = coffins[i].GetComponent<CoffinHandler>();
				}
				
				Vector3 end = coffins[i].FindChild("Target").position;
				Vector3 start = transform.position;
				
				_currentPathTarget = 0;
				_path = null;
				Scheduler.aksForPath( start, end, this);
				_waitingForPath = true;
			}
			if(_path != null)
			{
				_target = _path[_currentPathTarget];
			}
			else
			{
				base.newTarget();
			}	
		}
		else
		{
			
			_currentPathTarget = getNextReachable(_path, _currentPathTarget);
			_target = _path[ _currentPathTarget];
			_target.y = 1f;
			
		}		
	}
}
