using UnityEngine;
using System.Collections;

public class Power {

	protected float _timer = 0f;
	protected float _duration = 0f;
	protected float _coolDown = 0f;
	protected bool _activated = false;
	protected bool _ready = true;
	protected Player.PowerType _type;
	
	
	public Power( float duration, float cooldown)
	{
		_duration = duration;
		_coolDown = cooldown;
	}
	
	public virtual bool activate()
	{
		if(_ready)
		{
			_activated = true;		
			return true;
		}
		
		return false;
	}
	
	public virtual void guiPower()
	{
		bool bkEnabled = GUI.enabled;
		
		GUILayout.BeginHorizontal();
		
		GUILayout.FlexibleSpace();
		
		GUILayout.Box("", _type.ToString(), GUILayout.Width(60f), GUILayout.Height(60f));
		
		if(_ready && _activated)
		{
			GUILayout.HorizontalSlider(_timer, 0f, _duration, "powerSliderOn", "thumbSliderOn", GUILayout.Width(240f), GUILayout.Height(60f));
		}
		else if(_ready)
		{
			GUILayout.HorizontalSlider(_timer, 0f, _duration, "powerSliderOn", "thumbSliderOn", GUILayout.Width(240f), GUILayout.Height(60f));
		}
		else
		{
			GUILayout.HorizontalSlider(_timer, 0f, _coolDown, "powerSlider", "thumbSlider", GUILayout.Width(240f), GUILayout.Height(60f));
		}
		
		GUILayout.FlexibleSpace();
		
		GUILayout.EndHorizontal();
		
		GUI.enabled = bkEnabled;
	}
	
	public virtual void update( float dt)
	{
		if(_activated)
		{
			_timer += dt;
			if(_timer > _duration)
			{
				deactivate();
			}
		}
		else if(!_ready)
		{
			_timer += dt;
			if(_timer > _coolDown)
			{
				_ready = true;
				_timer = 0f;
			}
		}
	}
	
	public virtual void deactivate()
	{
		hide ();
		_ready = false;
		_activated = false;
		_timer = 0f;
	}
	
	public virtual void pause()
	{
		hide ();
		_activated = false;	
	}
	
	public bool isActivated() {
		return _activated;	
	}
	
	
	public Player.PowerType Type {
		get {
			return this._type;
		}
	}
	
	
	protected virtual void hide()
	{}
	
	public virtual void reset( float duration, float cooldown)
	{
		_duration = duration;
		_coolDown = cooldown;
	}
}
