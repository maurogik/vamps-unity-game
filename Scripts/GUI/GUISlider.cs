using UnityEngine;
using System.Collections;

public class GUISlider : GUIItem {
	
	

	public delegate void SetterType(int val);
	public delegate float GetterType();
	
	private GetterType _getter;
	private SetterType _setter;
	
	private string _label;
	private float _minVal;
	private float _maxVal;
	private float _sliderWidth;
	private float _sliderHeight;
	private float _labelWidth;
	private float _labelHeight;
	
	
	
	public GUISlider(string label, float minVal, float maxVal,
		float sliderWidth, float sliderHeight, float labelWidth, float labelHeight,
		GetterType getter, SetterType setter)
		: base("powerSliderOn")
	{
		_setter = setter;
		_getter = getter;
		
		_label = label;
		_minVal = minVal;
		_maxVal = maxVal;
		_sliderWidth = sliderWidth;
		_sliderHeight = sliderHeight;
		_labelWidth = labelWidth;
		_labelHeight = labelHeight;
	}
	
	
	public override void draw ()
	{
		base.draw ();
		
		GUILayout.BeginHorizontal();
		
		GUILayout.FlexibleSpace();
		
		GUILayout.Label(_label + _getter(), _selected ? "labelFontActive" : "labelFont", GUILayout.Width(_labelWidth * GUIManager.widthCoef)
			, GUILayout.Height(_labelHeight));
		
		GUILayout.FlexibleSpace();
		
		float tmp = (int)GUILayout.HorizontalSlider( _getter(),_minVal, _maxVal, _style
			, "thumbSliderOn", GUILayout.Width(_sliderWidth * GUIManager.widthCoef), GUILayout.Height(_sliderHeight));
		
		_position = GUILayoutUtility.GetLastRect();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		if(tmp != _getter())
		{
			GUIMenu.fireUnselection();
			setSelected(true);
		}
		
		_setter((int)tmp);
	}
	
	
	public override void forceValChange (float val)
	{
		base.forceValChange (val);
		float modif = val * (_maxVal - _minVal)/20f;
		if(Mathf.Abs(modif) < 1f)
		{
			modif = modif < 0 ? -1f : 1f;
		}
		float newVal = _getter() + modif;
		newVal = Mathf.Clamp(newVal, _minVal, _maxVal);
		_setter((int)newVal);
	}
}
