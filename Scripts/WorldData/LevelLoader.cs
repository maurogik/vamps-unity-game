using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {

	
	public string levelName;
	public Texture2D screen;
	
	void OnGUI()
	{
		if(Event.current.type == EventType.repaint)
		{
			GUI.DrawTexture(new Rect(0f, 0f, Screen.width, Screen.height), screen);	
			Application.LoadLevel(levelName);
			//Destroy(this.gameObject);
		}
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		//
	}
}
