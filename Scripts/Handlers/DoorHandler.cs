using UnityEngine;
using System.Collections;

public class DoorHandler : MonoBehaviour {
	
	
	public GameObject door;
	public Transform housePos;
	public float minStayInDuration = 3f;
	public float maxStayInDuration = 12f;

	
	private Transform _currentOccupant;
	private float _stayInDuration;
	private float _timer;
	
	void OnEnable()
	{
		door.SetActive(true);
	}
	
	void OnDisable()
	{
		door.SetActive(false);
		_currentOccupant = null;
	}
	
	public void setOccupant(Transform human)
	{
		_currentOccupant = human;
		_timer = 0f;
		float minStay, maxStay;
		
		DayNightController worldTimer = GameObject.Find("Core").GetComponent<WorldController>().worldTime;
		float hour = worldTimer.currentCycleTime;
		
		//night
		if(hour < worldTimer.dayCycleLength && hour > worldTimer.DuskTime)
		{
			hour = worldTimer.dayCycleLength - hour;
			minStay = hour/1.5f;
			maxStay = hour;
		}
		//day
		else
		{
			minStay = minStayInDuration;
			maxStay = maxStayInDuration;
		}
		
		_stayInDuration = Random.Range(minStay, maxStay);
		
		_currentOccupant.GetComponent<AICreatureControl>().enabled = false;
		_currentOccupant.GetComponent<Creature>().enabled = false;
	}
	
	
	// Update is called once per frame
	void Update ()
	{
		_timer += Time.deltaTime;
		
		if(_timer <= _stayInDuration)
		{
			//Do nothing
		}
		else
		{
			if(_currentOccupant != null)
			{			
				_currentOccupant.GetComponent<AICreatureControl>().enabled = true;
				Creature occupingCrea = _currentOccupant.GetComponent<Creature>();
				occupingCrea.enabled = true;
				occupingCrea.eat(Vector3.one);
			}
			_currentOccupant = null;
			enabled = false;
		}
	}
}
