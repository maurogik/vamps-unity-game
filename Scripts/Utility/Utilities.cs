using UnityEngine;
using System.Collections;

public class Utilities
{
	
	public static Transform RecursiveFindChild(Transform parent, string childName)
	{
		foreach(Transform child in parent)
		{
			if(child.name.Equals(childName))
			{
				return child;	
			}
			else
			{
				Transform res = RecursiveFindChild(child, childName);
				if(res && res.name.Equals(childName))
				{
					return res;	
				}
			}
		}
		return null;
	}
}
