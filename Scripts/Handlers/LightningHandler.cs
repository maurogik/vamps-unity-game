using UnityEngine;
using System.Collections.Generic;

public class LightningHandler : MonoBehaviour {
	
	public GameObject lightning;
	public GameObject lightningPrefab;
	public float lightningDuration = 5f;
	public float boltForce = 0.3f;
	public float maxDistance = 10f;
	public float yScaleGainCoef = 0.5f;
	
	public SoundManager.clipAndVolume lightningSound;
	
	private float _timer = 0f;
	private Creature _owner;

	
	void OnEnable()
	{
		_timer = 0f;
		
		EventHandler eventHandler = GetComponent<EventHandler>();
		if(!eventHandler.enabled)
		{
			eventHandler.enabled = true;
		}
		SoundManager.askForSoundPlay(transform, lightningSound);		
	}
	
	private List<Creature> findChainTargets(Vector3 start, float chainLength)
	{
		List<Creature> found = new List<Creature>();
		
		foreach(Collider hit in Physics.OverlapSphere(start, chainLength))
		{
			Creature crea = hit.gameObject.GetComponent<Creature>();
			
			if(crea != null)
			{
				found.Add(crea);
			}
		}
		
		return found;
	}
	
	private List<GameObject> chainBolts(LightningBolt fatherBolt, List<Creature> creatures)
	{
		
		List<GameObject> chained = new List<GameObject>();
		Transform parent = fatherBolt ? fatherBolt.target.transform : transform;
		
		foreach(Creature crea in creatures)
		{
			if(_owner.isPreyingOn(crea.CreatureType))
			{
				GameObject bolt	= Instantiate(lightningPrefab) as GameObject;
				bolt.transform.position = parent.position;
				bolt.transform.parent = parent;
				LightningBolt boltScript = bolt.GetComponent<LightningBolt>();
				boltScript.target = crea.transform;
				
				if(fatherBolt != null)
				{
					if(fatherBolt.chainedChildren == null)
					{
						fatherBolt.chainedChildren = new List<LightningBolt>();	
					}
					fatherBolt.chainedChildren.Add(boltScript);
				}
				chained.Add(bolt);

			}
		}
		
		return chained;
	}
	
	void OnDisable()
	{
		if(lightning != null)
		{
			lightning.GetComponent<LightningBolt>().cleanChainAndDestroy();
			lightning = null;
		}
	}
	
	void Update()
	{		
		_timer += Time.deltaTime;

		if(lightning != null)
		{
			if(!lightning.GetComponent<LightningBolt>().checkBoltValidity(maxDistance * _owner.scale.y))
			{
				lightning = null;	
			}
		}
		
		if(lightning == null)
		{
			_timer = lightningDuration;	
		}
		else
		{
			//Debug.Log((_target.transform.position - transform.position).magnitude);
			float modifier = (boltForce * Time.deltaTime);
			
			_owner.eat( lightning.GetComponent<LightningBolt>().eatFromChain(modifier) );
		}
		
		
		if(_timer >= lightningDuration)
		{
			_timer = 0f;
			enabled = false;
		}
	}
	
	public void setTarget(Creature target)
	{
		
		if(_owner == null)
		{
			_owner = GetComponent<Creature>();
		}
		
		if(lightning == null)
		{
			_timer = 0f;
			
			List<GameObject> tmpBolts;	
			List<Creature> tmpCrea = new List<Creature>();
			
			//add base bolt
			tmpCrea.Add(target);
			tmpBolts = chainBolts(null, tmpCrea);
			lightning = tmpBolts[0];
			
			//compose chain
			int nbChainMax = (int)Mathf.Pow(_owner.scale.y, 4);
			int i = 0;
			
			while( tmpBolts.Count < nbChainMax && i < tmpBolts.Count)
			{
				tmpCrea = findChainTargets(tmpBolts[i].transform.position, maxDistance * _owner.scale.y);
				tmpBolts.AddRange(chainBolts(tmpBolts[i].GetComponent<LightningBolt>(), tmpCrea));
				++i;
			}
			enabled = true;
			//Debug.Log("chain is : "+tmpBolts.Count+" long");
		}
	}
}
