using UnityEngine;
using System.Collections;

public class ThumbMapGUI : MonoBehaviour {
	
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
	{
		GUISkin bkSkin = GUI.skin;
		
		GUI.skin = GUIManager.s_skin;
		
		Rect mapRect = camera.rect;
		mapRect.x *= Screen.width;
		mapRect.y *= Screen.height;
		mapRect.width *= Screen.width;
		mapRect.height *= Screen.height;
		
		mapRect.y = Screen.height - mapRect.y - mapRect.height;

		GUI.Label(mapRect, "", "mapBorder");
		
		GUI.skin = bkSkin;
	}
}
