using UnityEngine;
using System.Collections;

public class EventHandler : MonoBehaviour {
	
	public static event System.Action<Creature> killAction;
	
	private float duration = 3f;
	private float animDuration = 1f;
	private float minScale = 1f;
	private float maxSclale = 2f;
	private float minAlpha = 0.1f;
	private float maxAlpha = 1f;
	private float guiIndicatorHalfSize = 10f;
	
	private GameObject _thumbMap;
	private float _timer = 0f;
	private float _alpha = 0f;
	private float _scale = 1f;
	
	public static void cleanListeners()
	{
		killAction = null;
	}
	
	void OnEnable()
	{
		if(killAction != null)
		{
			killAction(GetComponent<Creature>());	
		}
		if(_thumbMap == null)
		{
			_thumbMap = GameObject.Find("ThumbMap");	
		}
		_timer = 0f;
	}
	
	void OnDisable()
	{
		
	}
	
	void OnGUI()
	{

		if(_thumbMap != null)
		{			
			Rect mapRect = _thumbMap.camera.rect;
			mapRect.x *= Screen.width;
			mapRect.y *= Screen.height;
			mapRect.width *= Screen.width;
			mapRect.height *= Screen.height;
			
			Vector3 pos = _thumbMap.camera.WorldToScreenPoint(transform.position);
			
			if(pos.x > mapRect.x && pos.x < mapRect.x + mapRect.width &&
				pos.y > mapRect.y && pos.y < mapRect.y + mapRect.height)
			{
				pos.y = Screen.height - pos.y;
				GUISkin bkskin = GUI.skin;
				GUI.skin = GUIManager.s_skin;
				float guiHalfSize = guiIndicatorHalfSize * _scale;
				
				Rect area = new Rect(pos.x - guiHalfSize, pos.y - guiHalfSize,
					guiHalfSize * 2f, guiHalfSize * 2f);
				
				Color newCol = new Color(1f, 1f, 1f, _alpha);
				Color bkCol = GUI.color;
				
				GUI.color = newCol;
				
				GUI.Box(area, "", "killIndicator");
				
				GUI.skin = bkskin;
				GUI.color = bkCol;
			}
		}
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		_timer += Time.deltaTime;
		
		if(_timer > duration)
		{
			enabled = false;	
		}
		else
		{
			float animTime = _timer % animDuration;
			
			float startAlpha, endAlpha, startScale, endSclale;
			
			if(animTime < animDuration / 2f)
			{
				startAlpha = minAlpha;
				endAlpha = maxAlpha;
				startScale = maxSclale;
				endSclale = minScale;
			}
			else
			{
				startAlpha = maxAlpha;
				endAlpha = minAlpha;
				startScale = minScale;
				endSclale = maxSclale;
			}
			
			_alpha = Mathf.Lerp(startAlpha, endAlpha, animTime/animDuration);
			_scale = Mathf.Lerp(startScale, endSclale, animTime/animDuration);
		}
	}
}
