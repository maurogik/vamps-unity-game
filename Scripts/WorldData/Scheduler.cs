using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Scheduler : MonoBehaviour {
	
	
	private struct pathFindingData
	{
		public Vector3 startPos;
		public Vector3 goal;
		public AICreatureControl requester;
	}
	
	public NavigationMap pathfinder;
	public int maxConcurrentPathfinding = 5;
	public float targetFPS = 60f;
	
	public bool forceClearCalls = false;
	public bool debug = false;
	
	private static int _runningPathfinderCalls;
	private static Queue<pathFindingData> _pathfinderCalls;
	private static float _availableTimePerPathfinder = 1f;
	
	// Use this for initialization
	void Start () {
		_pathfinderCalls = new Queue<pathFindingData>();
	}
	
	// Update is called once per frame
	void Update () {
			
		if(debug)
		{
			Debug.Log("path queue size = " + _pathfinderCalls.Count);
			Debug.Log("running pathfinders : " + _runningPathfinderCalls);
		}
		
		if(forceClearCalls)
		{
			forceClearCalls = false;	
			_runningPathfinderCalls = 0;
		}
		
		for(int i = _runningPathfinderCalls; i < maxConcurrentPathfinding && _pathfinderCalls.Count != 0; ++i)
		{
			pathFindingData data = _pathfinderCalls.Dequeue();
			if(data.requester != null && data.requester.gameObject.activeSelf)
			{
				data.requester.StartCoroutine(pathfinder.getPathFromPos(data.startPos,
					data.goal, data.requester));
				++_runningPathfinderCalls;
			}
		}
		
		_availableTimePerPathfinder = (1f/targetFPS) / Mathf.Max(1f, _runningPathfinderCalls);
		//_availableTimePerPathfinder = 3f;
		//Debug.Log("available time for "+_runningPathfinderCalls+" pathfinders is "+_availableTimePerPathfinder);
	}
	
	public static void pathFindingOver()
	{
		//Debug.Log("path finding over");
		-- _runningPathfinderCalls;
		_runningPathfinderCalls = Mathf.Max(0, _runningPathfinderCalls);
	}
	
	public static void aksForPath(Vector3 startPos, Vector3 goal
		, AICreatureControl requester)
	{
		//Debug.Log("pathfinding call");
		if(requester.debugInfoOn)
		{
			Debug.Log("asking for path", requester.gameObject);	
		}
		
		pathFindingData data = new pathFindingData();
		data.startPos = startPos;
		data.goal = goal;
		data.requester = requester;
		
		_pathfinderCalls.Enqueue(data);
	}
	
	public static void clearCalls()
	{
		_pathfinderCalls.Clear();
	}

	public static float AvailableTimePerPathfinder {
		get {
			return _availableTimePerPathfinder;
		}
	}
}
