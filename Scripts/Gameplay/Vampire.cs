using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Vampire : Creature {


	public override void Awake()
	{
		_type = Type.Vampire;
		_preyedTypes = new List<Type>(){Type.Ghoul, Type.Human};
		_eatenScale = new Vector3(0.1f, 1.2f, 0.1f);
	}
}
