using UnityEngine;
using System.Collections.Generic;

public class BatsAIControl : MonoBehaviour {
	
	public int flockSize;
	
	public float xModifier = 10f;
	public float yModifier = 5f;
	public float zModifier = 10f;
	public float speed = 50f;
	public float radiusReach = 1f;
	
	public GameObject prefab;

	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnEnable()
	{
		Player p = GameObject.Find("player").GetComponent<Player>();
		
		//cancel player scale so that the flock scale is always the same
		transform.localScale = new Vector3(1f/p.scale.x, 1f/p.scale.y, 1f/p.scale.z);
		flockSize = Mathf.Max(1, (int) Mathf.Pow( p.scale.y, 6));
		for(int i =0; i < flockSize; ++i)
		{
			GameObject bat = Instantiate(prefab, Random.insideUnitSphere * xModifier, Quaternion.identity) as GameObject;
			bat.transform.parent = transform;
			bat.tag = "bat";
		}
	}
	
	void OnDisable()
	{

	    List<GameObject> children = new List<GameObject>();
	    foreach (Transform child in transform) children.Add(child.gameObject);
	    foreach (GameObject child in children) 
		{	
			if(child.tag == "bat")
			{
				Destroy(child);
			}
		}
	}
}
