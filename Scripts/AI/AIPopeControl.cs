using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// AI pope control.
/// </summary>
public class AIPopeControl : AICreatureControl {
	
	public List<Transform> slots;
	public System.Action popeDeadEvent;
	
	
	private int _lastRequestedSlot = -1;
	
	// Update is called once per frame
	public override void Update () {
	 	base.Update();
		_lastRequestedSlot = -1;
	}
	
	protected override void encounter ()
	{
		if(_lastCreatureHit.CreatureType == Creature.Type.Cleric)
		{
		}
		else if(_lastCreatureHit.CreatureType != Creature.Type.Human)
		{	
			if((_lastCreatureHit.transform.position - transform.position).magnitude < actDistance)
			{
				act();	
			}
			//_toFollow = _lastCreatureHit;
		}
	}
	
	public Vector3 requestTarget()
	{
		++_lastRequestedSlot;
		if(_lastRequestedSlot < slots.Count)
		{
			return slots[_lastRequestedSlot].position;
		}
		else
		{
			return transform.position;	
		}
	}
	

	
	public override void newTarget ()
	{
		if(_path == null || _currentPathTarget >= _path.Count - 1)
		{
			
			GameObject AI = GameObject.Find("AI");
			MapGenerator map = AI.GetComponent<MapGenerator>();
			List<Transform> blocks = new List<Transform>();
			blocks.AddRange(map.getBlocks());
			
			//find something to reach
			int i = Random.Range(0, blocks.Count - 1);
			
			Vector3 end = blocks[i].position;
			Vector3 start = transform.position;
			
			_currentPathTarget = 0;

			_path = null;
			Scheduler.aksForPath( start, end, this);
			
			if(_path != null)
			{
				_target = _path[_currentPathTarget];
			}
			else
			{
				base.newTarget();
			}		
		}
		else
		{
			_currentPathTarget = getNextReachable(_path, _currentPathTarget);
			_target = _path[ _currentPathTarget];
			_target.y = 1f;
			
		}		
	}
	
	/*public override bool act ()
	{
		LightningHandler boltHandler = GetComponent<LightningHandler>();
		boltHandler.setTarget(_lastCreatureHit);
		boltHandler.enabled = true;
		
		return false;
	}*/
	
	public override void kill ()
	{
		if(popeDeadEvent != null)
		{
			popeDeadEvent();	
		}
		base.kill();
	}

}
