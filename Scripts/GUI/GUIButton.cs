using UnityEngine;
using System.Collections;

public class GUIButton : GUIItem {
	
	
	public delegate void SetterBool(bool b);
	
	private string _content;
	private float _buttonHeight;
	private float _buttonWidth;
	private SetterBool _setter;
	
	
	public GUIButton(string text, float width, float height, SetterBool setter)
		: base("button")
	{
		_content = text;
		_buttonHeight = height;
		_buttonWidth = width;
		_setter = setter;
	}
	
	
	public override void draw ()
	{
		base.draw ();
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		bool tmp = GUILayout.Button(_content, _style, GUILayout.Height(_buttonHeight * GUIManager.heightCoef)
			, GUILayout.Width(_buttonWidth * GUIManager.widthCoef));
		
		_position = GUILayoutUtility.GetLastRect();
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		if(tmp)
		{
			GUIMenu.fireUnselection();
			_selected = true;
		}
		
		_setter(tmp);
	}
	
	public override void forceAction ()
	{
		base.forceAction ();
		_setter(true);
		GUIMenu.fireUnselection();
	}
}
