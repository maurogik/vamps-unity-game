using UnityEngine;
using System.Collections;

/// <summary>
/// AI ghost control.
/// </summary>
public class AIGhostControl : AICreatureControl {
	
	
	private float _floatDownTimer = 0f;
	private float _floatDownDuration = 15f;
	
	public override void newTarget()
	{
		//ghost choose a random target
		_target = Random.insideUnitSphere;
		_target.x *= xModif;
		_target.y = 0f;
		_target.z *= zModif;
		_target += transform.position;
	}
	
	
	protected override void collisionOnPath (RaycastHit hit)
	{
		//A ghost doesn't care about wall, except if they are the map extern walls
		if(hit.collider.gameObject.tag == "externWall")
		{
			base.collisionOnPath(hit);
		}
	}
	
	public override void Update ()
	{
		base.Update ();
		
		
		if(transform.position.y > _creature.scale.y)
		{
			_floatDownTimer += Time.deltaTime;
			
			if(_floatDownTimer > _floatDownDuration)
			{
				_floatDownTimer = 0f;	
			}
			
			float posY = Mathf.Lerp(transform.position.y, _creature.scale.y, _floatDownTimer/_floatDownDuration);
			
			Vector3 pos = transform.position;
			pos.y = posY;
			transform.position = pos;
		}
	}
}
