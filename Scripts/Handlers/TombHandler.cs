using UnityEngine;
using System.Collections;

public class TombHandler : MonoBehaviour {
	
	
	public float getInDuration = 2f;
	public float getOutDuration = 2f;
	public float minStayInDuration = 3f;
	public float maxStayInDuration = 8f;
	public Vector3 targetPos = Vector3.zero;
	
	private Transform _currentGhoul;
	private float _stayInDuration;
	private float _timer;
	private Vector3 _originalGhoulPos;
	
	void OnEnable()
	{
	}
	
	void OnDisable()
	{
		_currentGhoul = null;
	}
	
	public void setOccupant(Transform ghoul)
	{
		_currentGhoul = ghoul;
		_originalGhoulPos = ghoul.position;
		_timer = 0f;
		
		float minStay, maxStay;
		
		DayNightController worldTimer = GameObject.Find("Core").GetComponent<WorldController>().worldTime;
		float hour = worldTimer.currentCycleTime;
		
		//night
		if(hour < worldTimer.dayCycleLength && hour > worldTimer.DuskTime)
		{
			minStay = minStayInDuration;
			maxStay = maxStayInDuration;
		}
		//day
		else
		{
			hour = worldTimer.DuskTime - hour;
			minStay = hour/1.5f;
			maxStay = hour;
		}
		
		_stayInDuration = Random.Range(minStay, maxStay);
		_currentGhoul.GetComponent<AICreatureControl>().enabled = false;
		_currentGhoul.GetComponent<Creature>().enabled = false;
	}
	
	
	// Update is called once per frame
	void Update ()
	{
		if(_currentGhoul == null)
		{
			enabled = true;	
			return;
		}
		_timer += Time.deltaTime;
		
		if(_timer <= getInDuration)
		{
			_currentGhoul.position = Vector3.Lerp(_originalGhoulPos,
				transform.TransformPoint(targetPos), _timer/getInDuration);
		}
		else if(_timer <= getInDuration + _stayInDuration)
		{
			_currentGhoul.gameObject.SetActive(false);
		}
		else if(_timer <= getInDuration + _stayInDuration + getOutDuration)
		{
			_currentGhoul.gameObject.SetActive(true);
			Vector3 pos = transform.position;
			pos.y = _originalGhoulPos.y;
			_originalGhoulPos = pos;
			_currentGhoul.position = Vector3.Lerp(transform.TransformPoint(targetPos),
				_originalGhoulPos, (_timer - getInDuration - _stayInDuration)/getOutDuration);
		}
		else 
		{
			_currentGhoul.GetComponent<AICreatureControl>().enabled = true;
			Creature occupingCrea = _currentGhoul.GetComponent<Creature>();
			occupingCrea.enabled = true;
			occupingCrea.eat(Vector3.one);
			enabled = false;
		}
	}
}
