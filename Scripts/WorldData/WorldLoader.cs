using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldLoader : MonoBehaviour {
	
	public static string loaderText = "loading";
	

	public AIGlobalControl aiGlobal;
	public MapGenerator mapGenerator;
	public NavigationMap navMapMaker;
	public WorldController worldControl;
	
	private List<LoadingScript> _toStart;
	private bool _started = false;
	
	
	private IEnumerator loadWorldData()
	{
		_toStart = new List<LoadingScript>(){mapGenerator, navMapMaker, aiGlobal};
		foreach(LoadingScript script in _toStart)
		{
			script.enabled = true;
			while(script.loadginDone == false)
			{	
				yield return new WaitForSeconds(1);	
			}
		}
		enabled = false;
		if(!_started)
		{
			StartCoroutine(worldControl.startGame());
			_started = true;
		}
		
	}
	
	
	// Update is called once per frame
	void Start () {
		StartCoroutine(loadWorldData());
	}
	
	void OnEnable()
	{
		//_started = false;	
	}
	
	public string getLoaderText()
	{
		return loaderText;	
	}
	
	
	/*void OnGUI()
	{
		GUI.skin = skin;
		
		if(!_loadingStarted || !_loadingOver)
		{
			
			
			float areaWidth = 680f * widthCoef;
			float areaHeight = 800f * heightCoef;
			float areaX = 625f * widthCoef;
			float areaY = 225f * heightCoef;
			
			float buttonHeight = 100f * heightCoef;
			
			GUI.DrawTexture(new Rect(0f, 0f, Screen.width, Screen.height), loadingTexture);
			GUILayout.BeginArea(new Rect(areaX, areaY, areaWidth, areaHeight));
			GUILayout.BeginVertical();
			
			GUILayout.FlexibleSpace();
			
			if(!_loadingStarted)
			{
				float labelHeight = 60f * heightCoef;
				float labelWidth = 300f * widthCoef;
				
				float sliderWidth = 350f * widthCoef;
				float sliderHeight = 60f;
				
				
				aiGlobal.nbHumanMax = labelAndSlider("Number of humans : ", aiGlobal.nbHumanMax, 0, 200,
					sliderWidth, sliderHeight, labelWidth, labelHeight);
				
				aiGlobal.nbGhoulMax = labelAndSlider("Number of ghouls : ", aiGlobal.nbGhoulMax, 0, 50,
					sliderWidth, sliderHeight, labelWidth, labelHeight);
				
				aiGlobal.nbVampireMax = labelAndSlider("Number of vamps : ", aiGlobal.nbVampireMax, 0, 50,
					sliderWidth, sliderHeight, labelWidth, labelHeight);
				
				aiGlobal.nbGhostMax = labelAndSlider("Number of ghosts : ", aiGlobal.nbGhostMax, 0, 50,
					sliderWidth, sliderHeight, labelWidth, labelHeight);
				
				aiGlobal.nbSquadMax = labelAndSlider("Number of squads : ", aiGlobal.nbSquadMax, 0, 15,
					sliderWidth, sliderHeight, labelWidth, labelHeight);
				
				mapGenerator.nbHumanHouse = labelAndSlider("Number of houses : ", mapGenerator.nbHumanHouse, 0, 100,
					sliderWidth, sliderHeight, labelWidth, labelHeight);
				
				mapGenerator.nbTomb = labelAndSlider("Number of tombs : ", mapGenerator.nbTomb, 0, 50,
					sliderWidth, sliderHeight, labelWidth, labelHeight);
				
				mapGenerator.nbCoffin = labelAndSlider("Number of coffins : ", mapGenerator.nbCoffin, 0, 50,
					sliderWidth, sliderHeight, labelWidth, labelHeight);
				
				GUILayout.FlexibleSpace();
	
				_loadingStarted = GUILayout.Button("Start playing", "button", GUILayout.Width(areaWidth),
					GUILayout.Height(buttonHeight));
				
				//GUILayout.Space(10f);
				
				if(GUILayout.Button("Exit game", "button", GUILayout.Width(areaWidth),
					GUILayout.Height(buttonHeight)))
				{
					Application.Quit();
				}
			}
			else if(!_loadingOver)
			{
				GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				GUILayout.Label(loaderText, "loadingText");
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
			
			GUILayout.FlexibleSpace();
			
			GUILayout.EndVertical();
			GUILayout.EndArea();
			
			
		}
		else
		{
			if(_displaySettings)
			{
				//_quitGame |= aiGlobal.AIGui();	
			}
		}
	}*/
	
}
