using UnityEngine;
using System.Collections;

public class BatDash : Power {

	
	private GameObject _normalShape;
	private GameObject _batShape;
	
	public BatDash(float duration, float cooldown)
		: base(duration, cooldown)
	{
		_normalShape = GameObject.Find("NormalShape");
		_batShape = GameObject.Find("BatShape");
		_batShape.SetActive(false);
		_type = Player.PowerType.BAT_DASH;
	}
	
	public override bool activate()
	{
		if(base.activate())
		{
			GameObject player = GameObject.Find("player");
			Vector3 playerPos = player.transform.position;
			playerPos.y = 1f;
			//player.transform.position = playerPos;
			_normalShape.SetActive(false);
			_batShape.SetActive(true);
			
			return true;
		}
		
		return false;
	}
	
	protected override void hide()
	{
		_normalShape.SetActive(true);
		_batShape.SetActive(false);
	}
	

}
