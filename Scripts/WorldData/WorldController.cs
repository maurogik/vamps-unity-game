using UnityEngine;
using System.Collections;

public class WorldController : MonoBehaviour {
	
	public AIGlobalControl aiGlobal;
	public MapGenerator mapGenerator;
	public NavigationMap navMap;
	public DayNightController worldTime;
	public GameObject playerContainer;
	public GameObject eyeContainer;
	public Transform playerContainerPrefab;
	public Transform eyeContainerPrefab;
	
	private bool _gameRunning = false;
	private bool _gameStarted = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public bool isPlayerDead()
	{
		return playerContainer.transform.childCount < 2;	
	}
	
	public void pauseGame(bool paused)
	{
		_gameRunning = !paused;
		
		PlayerControl control = playerContainer.GetComponentInChildren<PlayerControl>();
		Player play = playerContainer.GetComponentInChildren<Player>();
		SmoothFollowCam camControl = playerContainer.GetComponentInChildren<SmoothFollowCam>();
		
		if(paused)
		{
			aiGlobal.pauseAllAI(false);
			control.enabled = false;
			
			if(camControl != null)
			{
				camControl.enabled = false;
			}
			
			play.enabled = false;
		}
		else
		{
			aiGlobal.pauseAllAI(true);
			control.enabled = true;
			
			if(camControl != null)
			{
				camControl.enabled = true;
			}
			
			play.enabled = true;
		}
	}
	
	public void resetGame()
	{
		SoundManager.clearSources();
		aiGlobal.resetAllAI();
		Scheduler.clearCalls();
		_gameRunning = true;
	}
	
	public IEnumerator startGame()
	{
		_gameRunning = true;
		_gameStarted = true;
		//aiGlobal.resetAllAI();
		
		if(eyeContainer != null)
		{
			Destroy(eyeContainer);
		}
		
		eyeContainer = (Instantiate(eyeContainerPrefab) as Transform).gameObject;
		eyeContainer.name = "EyeContainer";
		
		if(playerContainer != null)
		{
			Destroy(playerContainer);	
		}
		
		yield return null;
		
		playerContainer = (Instantiate(playerContainerPrefab) as Transform ).gameObject;
		playerContainer.name = "PlayerContainer";
		
		playerContainer.GetComponentInChildren<Player>().initPowers();
	}

	public bool GameRunning {
		get {
			return this._gameRunning;
		}
	}

	public bool GameStarted {
		get {
			return this._gameStarted;
		}
	}
}
