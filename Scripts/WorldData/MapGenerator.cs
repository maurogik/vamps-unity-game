using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator : LoadingScript {
	
	public float minHouseScale = 0.7f;
	public float maxHouseScale = 2f;
	public float minTombScale = 1f;
	public float maxTombScale = 1.5f;
	public float minCoffinScale = 1f;
	public float maxCoffinScale = 1.5f;
	public float houseHalfWidth = 10f;
	public float tombHalfWidth = 1f;
	public float coffinHalfWidth = 1f;
	public float castleHalfWidth = 70f;
	public float churchHalfWidth = 20f;
	public float mapHalfWidth;
	public float mapHalfDepth;
	public Transform humanHousePrefab;
	public Transform tombPrefab;
	public Transform coffinPrefab;
	public Transform castlePrefab;
	public Transform churchPrefab;
	public int nbHumanHouse;
	public int nbTomb;
	public int nbCoffin;
	
	private List<Transform> _cityBlocks;
	private List<Transform> _houses;
	private List<Transform> _tombs;
	private List<Transform> _coffins;
	
	private Dictionary<Transform, float> _blockAndSize;
	
	private Transform _castle;
	
	// Use this for initialization
	void Start () {

		Debug.Log("starting map");
		StartCoroutine(generateAllBlocks());
	}
	
	void Awake()
	{

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	IEnumerator generateAllBlocks()
	{
		_blockAndSize = new Dictionary<Transform, float>();
		
		_cityBlocks = new List<Transform>();
		_houses = new List<Transform>();
		_tombs = new List<Transform>();
		_coffins = new List<Transform>();
		
		GameObject level = GameObject.Find("level");
		
		_castle = generateBlock(castleHalfWidth, 1f, 1f, castlePrefab, level.transform);
		_blockAndSize[_castle] = castleHalfWidth;
		
		Transform church = generateBlock(churchHalfWidth, 0.8f, 1.5f, churchPrefab, level.transform);
		_cityBlocks.Add(church);
		_blockAndSize[church] = churchHalfWidth;
		
		Transform housesParent = level.transform.FindChild("Houses");
		for(int i = 0; i < nbHumanHouse; ++i)
		{
			Transform house = generateBlock(houseHalfWidth, minHouseScale, maxHouseScale, humanHousePrefab, housesParent);
			_cityBlocks.Add(house);
			_houses.Add(house);
			_blockAndSize[house] = houseHalfWidth;
			WorldLoader.loaderText = "House "+(i+1)+" out of "+nbHumanHouse+" generated";
			yield return null;
		}
		
		Transform tombsParent = level.transform.FindChild("Tombs");
		for(int i = 0; i < nbTomb; ++i)
		{
			Transform tomb = generateBlock(tombHalfWidth, minTombScale, maxTombScale, tombPrefab, tombsParent);
			_cityBlocks.Add(tomb);
			_tombs.Add(tomb);
			_blockAndSize[tomb] = tombHalfWidth;
			WorldLoader.loaderText = "Tomb "+(i+1)+" out of "+nbTomb+" generated";
			yield return null;
		}
		
		Transform coffinsParent = level.transform.FindChild("Coffins");
		for(int i = 0; i < nbCoffin; ++i)
		{
			Transform coffin = generateBlock(coffinHalfWidth, minCoffinScale, maxCoffinScale, coffinPrefab, coffinsParent);
			_cityBlocks.Add(coffin);
			_coffins.Add(coffin);
			_blockAndSize[coffin] = coffinHalfWidth;
			WorldLoader.loaderText = "Coffin "+(i+1)+" out of "+nbCoffin+" generated";
			yield return null;
		}
		
		_blockAndSize.Clear();
		_blockAndSize = null;
		Debug.Log("map generated");
		loadginDone = true;
	}
	
	private Vector3 generateScale(float minBlockScale, float maxBlockScale)
	{
		Vector3 scale = Vector3.zero;
		scale.x = Random.Range(minBlockScale, maxBlockScale);
		scale.y = Random.Range(minBlockScale, maxBlockScale);
		scale.z = Random.Range(minBlockScale, maxBlockScale);
		
		return scale;
	}
	
	private Transform generateBlock(float blockHalfWidth, float minScale, float maxScale, Transform blockPrefab, Transform parent)
	{
		Vector3 res = Random.insideUnitSphere;
		
		float maxPosX = (mapHalfWidth - blockHalfWidth * maxScale);;
		float maxPosZ = (mapHalfDepth - blockHalfWidth * maxScale);
		
		res.y = 0f;
		res.x *= maxPosX;
		res.z *= maxPosZ;
		
		Vector3 scale = generateScale(minScale, maxScale);
		
		while(isOverlapping(res, scale, blockHalfWidth))
		{
			res = Random.insideUnitSphere;
			res.y = 0f;
			res.x *= maxPosX;
			res.z *= maxPosZ;
			scale = generateScale(minScale, maxScale);
		}
		
		float rotat = Random.value * 360f;
		
		Transform block = Instantiate(blockPrefab, res, Quaternion.Euler(0f, rotat, 0f)) as Transform;
		block.localScale = scale;
		block.parent = parent;
		
		return block;
	}
	
	private bool isOverlapping(Vector3 pos, Vector3 scale, float blockHalfWidth)
	{
		float maxScale = Mathf.Sqrt(scale.x * scale.x + scale.z * scale.z);
		//Debug.Log("max scale : "+maxScale);
		foreach(Transform block in _cityBlocks)
		{			
			float maxBlockScale = Mathf.Sqrt(block.localScale.x * block.localScale.x + block.localScale.z * block.localScale.z);
			float testedBlockHalfWidth = _blockAndSize[block];
			if((block.position - pos).magnitude < (maxScale* blockHalfWidth) + maxBlockScale * testedBlockHalfWidth )
			{
				//Debug.Log("overlap refused");
				return true;	
			}
		}
		
		return false;	
	}
	
	public List<Transform> getBlocks()
	{
		return _cityBlocks;	
	}
	
	public List<Transform> getHouses()
	{
		return _houses;
	}
	
	public List<Transform> getTombs()
	{
		return _tombs;	
	}
	
	public List<Transform> getCoffins()
	{
		return _coffins;	
	}
	
	
	public float getNbHouses()
	{
		return nbHumanHouse;	
	}
	
	public float getNbCoffins()
	{
		return nbCoffin;	
	}
	
	public float getNbTombs()
	{
		return nbTomb;	
	}
	
	public void setNbHouses(int val)
	{
		nbHumanHouse = val;
	}
	
	public void setNbTombs(int val)
	{
		nbTomb = val;
	}
	
	public void setNbCoffins(int val)
	{
		nbCoffin = val;
	}
}
