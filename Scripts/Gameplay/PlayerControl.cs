using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControl : AICreatureControl {
	
	
	
	public Player _player;
	public float jumpHeight;
	public float camSensibilty = 5f;
	
	private bool _jumped = false;
	private bool _jumpForceApplied = false;
	private bool _controlled = true;
	private Transform _playerBody;
	private float _bkupSpeed;
	private bool _hovering = false;
	private Vector3 _move;
	
	private GameObject _lastSelected = null;
	private GameObject _selectionObject = null;
	
	//houseInsideHandling
	private List<TransparencyHandler> _lastHouseWalls = null; 
	private Vector3 _direction; //player/camera vector
	
	
	// Use this for initialization
	public override void Start () {
		base.Start();
		EventHandler.killAction -= catchEvent;
		_playerBody = transform.Find("NormalShape");
		_bkupSpeed = maxSpeed;
		_lastHouseWalls = new List<TransparencyHandler>();
	}
	
	// Update is called once per frame
	public override void Update () {
		
		runRaycastCheck();
		
		if(InputManager.input.getButtonDown("Run") && !_player.isActivated(Player.PowerType.BAT_DASH) && !_player.isHolding())
		{
			_player.activatePower(Player.PowerType.BAT_DASH);
		}
		else if(InputManager.input.getButtonDown("Run") && _player.isActivated(Player.PowerType.BAT_DASH))
		{
			_player.deactivatePower(Player.PowerType.BAT_DASH);
		}
		
		if(_player.isActivated(Player.PowerType.BAT_DASH))
		{
			maxSpeed = _bkupSpeed * 5f;
		}
		else if(_player.isActivated(Player.PowerType.GHOST_SHAPE))
		{
			maxSpeed = _bkupSpeed/2f;
		}
		else
		{
			maxSpeed = _bkupSpeed;
		}
		

		if(_jumped)
		{
			maxSpeed = _bkupSpeed * 2f;
		}
		
		_move = Vector3.zero;
		
		if(_controlled)
		{
			newTarget();
			
			
			if(InputManager.input.getButtonDown("Jump") && !_jumped && !_player.isPowerRunnng())
			{
				GetComponent<CreatureSoundHandler>().playBaseSound(true);//base sound is jumps sound for the player
				_jumped = true;
				_jumpForceApplied = false;
			}
			else if(InputManager.input.getButton("Jump") && _jumped)
			{
				_hovering = true;
			}
			else
			{
				_hovering = false;
			}
			
			rotateTransformFromInput();
			Vector3 move = getMovement() * Time.deltaTime;
			Vector3 newPos = move + transform.position;
			_move = move/Time.deltaTime;
			bentFromMovement(_move, _playerBody);
			transform.position = newPos;
		}
		
		if(InputManager.input.getButtonDown("Power2") && !_player.isActivated(Player.PowerType.THIRD_EYE)
			 && !_player.isHolding())
		{
			_player.activatePower(Player.PowerType.THIRD_EYE);
		}
		else if(InputManager.input.getButtonDown("Power2") && _player.isActivated(Player.PowerType.THIRD_EYE))
		{
			_player.deactivatePower(Player.PowerType.THIRD_EYE);
		}
		
		if(InputManager.input.getButtonDown("Power1") && !_player.isActivated(Player.PowerType.GHOST_SHAPE)
			 && !_player.isHolding())
		{
			_player.activatePower(Player.PowerType.GHOST_SHAPE);
		}
		else if(InputManager.input.getButtonDown("Power1") && _player.isActivated(Player.PowerType.GHOST_SHAPE))
		{
			_player.deactivatePower(Player.PowerType.GHOST_SHAPE);	
		}
		
		if (InputManager.input.getButtonDown("Attack") && !_player.isPowerRunnng())
		{
			act();
		}
		
		if (InputManager.input.getButtonDown("Catch")&& !_player.isPowerRunnng())
		{
			if(_player.isHolding())
			{
				_player.releaseHold();	
			}
			else
			{
				if(_lastCreatureHit != null && _selectionObject != null)
				{
					_player.startHolding(_lastCreatureHit);
					//flee sound is capture sound for the player
					GetComponent<CreatureSoundHandler>().playFleeSound();
				}
			}
		}
		
		
		//Selection part
		
		if(_selectionObject && _lastObjectHit != _lastSelected)
		{	
			_selectionObject.SetActive(false);
			_selectionObject = null;
			_lastSelected = null;
		}
		
		if(_lastObjectHit != null && !_lastObjectHit.tag.Equals("Player") && !_player.isPowerRunnng() && _selectionObject == null)
		{
			Transform selectTransf = Utilities.RecursiveFindChild(_lastObjectHit.transform, "Select");
			_selectionObject = selectTransf ? selectTransf.gameObject : null;
			if(_selectionObject)
			{
				_selectionObject.SetActive(true);
				_lastSelected = _lastObjectHit;
			}
		}
		
		
		//house inside part
		
		Debug.DrawRay(transform.position, Camera.main.transform.position - transform.position, Color.green);
		
		foreach(TransparencyHandler wall in _lastHouseWalls)
		{
			wall.switchOff();
		}
		_lastHouseWalls.Clear();
		
		_direction = Camera.main.transform.position - transform.position;
		
		foreach(RaycastHit hit in Physics.RaycastAll(transform.position,
			_direction, _direction.magnitude))
		{
			TransparencyHandler transpHandler = hit.collider.gameObject.GetComponent<TransparencyHandler>();
			if(transpHandler)
			{
				transpHandler.switchOn();
				_lastHouseWalls.Add(transpHandler);
			}
		}
		
		if(_player.isDead())
		{
			kill();	
		}
	}
	
	//Physic must be applied in fixed update
	void FixedUpdate()
	{
		if(_jumped && !_jumpForceApplied)
		{
			_jumpForceApplied = true;
			rigidbody.AddForce(transform.up * jumpHeight * _creature.getSpeedRatio(), ForceMode.VelocityChange);
		}
		if(_hovering)
		{
			rigidbody.AddForce(-Physics.gravity/2f * Time.deltaTime, ForceMode.VelocityChange);
		}
	}
	
	
	public override bool act()
	{
		if(_player.isPowerRunnng())
		{
	
		}
		else if(_lastObjectHit != null && _lastObjectHit.GetComponent<LevarHandler>() != null)
		{
			LevarHandler levar = _lastObjectHit.GetComponent<LevarHandler>();
			return levar.switchLevar();
		}
		else if(_lastCreatureHit != null)
		{
			LightningHandler boltHandler = GetComponent<LightningHandler>();
			boltHandler.setTarget(_lastCreatureHit);
		}
		return false;
	}

	protected override void handleHit (GameObject objectHit, Creature creatureHit)
	{
		_lastObjectHit = objectHit;
		_lastCreatureHit = creatureHit;
	}
	
	protected override Vector3 getMovement ()
	{
		Vector3 move = base.getMovement ();
		return move;
	}

	
	public override void newTarget ()
	{
		float h,v;
		h = InputManager.input.getMouvementX();
		v = InputManager.input.getMouvementY();
		
		Vector3 forward= transform.forward;
		
		Vector3 right= transform.right;
		
		Vector3 direction= h * right + v * forward;
		
		_target = transform.position + direction * maxSpeed;
	}
	
	private void rotateTransformFromInput()
	{
		float camH, camV;
		
		camH = InputManager.input.getCameraX();
		camV = InputManager.input.getCameraY();
		
		Transform toRotate = transform;

		camV *= camSensibilty * Time.deltaTime;
		camH *= camSensibilty * Time.deltaTime;
		
		toRotate.Rotate(new Vector3(0f, camH, 0f));

	}

	
	void OnCollisionEnter(Collision collision) {
		
		if(collision.gameObject.tag == "ground" || collision.gameObject.tag == "wall" || collision.gameObject.tag == "roof")
		{
			_jumped = false;	
		}
	}
	
	void OnCollisionStay(Collision collision) {
		if(collision.gameObject.tag == "ground" || collision.gameObject.tag == "wall" || collision.gameObject.tag == "roof")
		{
			_jumped = false;	
		}
	}
	
	void OnCollisionExit(Collision collision) {
		if(collision.gameObject.tag == "ground" || collision.gameObject.tag == "wall" || collision.gameObject.tag == "roof")
		{
			_jumped = true ;	
		}
	}
	
	
	public override void kill ()
	{
		base.kill ();
		Debug.Log("killing player");
		GameObject.Find("Core").GetComponent<GUIManager>().setDisplayDeathMenu(true);
		GameObject.Find("Core").GetComponent<WorldController>().pauseGame(true);
	}

	public bool Controlled {
		get {
			return this._controlled;
		}
		set {
			_controlled = value;	
		}
	}
}



