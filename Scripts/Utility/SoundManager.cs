using UnityEngine;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {
	
	[System.Serializable]
	public class clipAndVolume
	{
		public AudioClip clip;
		public float volume;
	}
	
	public AudioSource musicSource;
	public List< clipAndVolume > dayMusics;
	public List< clipAndVolume > nightMusics;
	
	
	private List< clipAndVolume > _musicPool;
	private GameObject _player;
	private GameObject _eyeContainer;
	
	
	public static float soundPower = 5f;
	private static SoundManager _manager;
	private static float _hearingDistance = 75f;
	
	private static  int _maxConcurrentSound = 10;
	private static List<AudioSource> _currentSources;
		
	// Use this for initialization
	void Start () {
		DayNightController.dawnStartEvent += dawnStarting;
		DayNightController.duskStartEvent += duskStarting;
		_musicPool = dayMusics;
		
		_manager = this;
		
		_currentSources = new List<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if(_player == null)
		{
			_player = GameObject.Find("player");	
		}
		
		if(_eyeContainer == null)
		{
			_eyeContainer = GameObject.Find("EyeContainer");
		}
		
		if(_player != null)
		{
			if(!_player.activeInHierarchy)
			{
				musicSource.transform.position = _eyeContainer.transform.position;
			}
			else
			{
				musicSource.transform.position = _player.transform.position;
			}
		}
		
		if(!musicSource.isPlaying)
		{
			Debug.Log("new music");
			pickNewMusic();
		}
		
	}
	
	void OnDestroy()
	{
		DayNightController.dawnStartEvent -= dawnStarting;
		DayNightController.duskStartEvent -= duskStarting;
	}
	
	private void pickNewMusic()
	{
		if(musicSource.isPlaying)
		{
			musicSource.Stop();	
		}
		
		clipAndVolume newMusic;
		
		int clipInd = Random.Range(0, _musicPool.Count - 1);
		
		newMusic = _musicPool[clipInd];
		
		musicSource.clip = newMusic.clip;
		musicSource.volume = newMusic.volume;
		musicSource.Play();
	}
	
	private void duskStarting()
	{
		_musicPool = nightMusics;
		pickNewMusic();
	}
	
	private void dawnStarting()
	{
		_musicPool = dayMusics;
		pickNewMusic();
	}

	public static bool isPlayAuthorized()
	{
		List<AudioSource> toRemove = new List<AudioSource>();
		foreach(AudioSource source in _currentSources)
		{
			if(!source.isPlaying)
			{
				toRemove.Add(source);	
			}
		}
		
		foreach(AudioSource toRm in toRemove)
		{
			_currentSources.Remove(toRm);	
		}
		
		return _currentSources.Count < _maxConcurrentSound;
	}
	
	public static void removeSource(AudioSource source)
	{
		_currentSources.Remove(source);	
	}
	
	public static void clearSources()
	{
		_currentSources.Clear();	
	}
	
	public static void askForSoundPlay(Transform origin, SoundManager.clipAndVolume sound, bool forcePlay = false)
	{
		float distance = (_manager.transform.position - origin.position).magnitude;
		if(distance < _hearingDistance
			&& (forcePlay || isPlayAuthorized()))
		{
			AudioSource source = origin.audio;
			if(source != null && !source.isPlaying)
			{
				source.clip = sound.clip;
				source.volume = (sound.volume * soundPower) /* / ratio*/;
				source.Play();
				
				if(!forcePlay)
				{
					_currentSources.Add(source);	
				}
			}
		}
	}
}
