using UnityEngine;
using System.Collections;

public class MainMenu : GUIMenu {

	
	public MainMenu()
	 : base()
	{
		initMenu();
	}
	
	
	private void initMenu()
	{
		float buttonHeight = 100f;
		float buttonWidth = 500f;
		GUIManager gui = GameObject.Find("Core").GetComponent<GUIManager>();
		
		_items.Add(new GUIButton("New game", buttonWidth, buttonHeight, new GUIButton.SetterBool(gui.setNewGame)));
		
		_items.Add(new GUIButton("Help", buttonWidth, buttonHeight, new GUIButton.SetterBool(gui.setHelpMenu)));
		
		_items.Add(new GUIButton("Credit", buttonWidth, buttonHeight, new GUIButton.SetterBool(gui.setCreditsMenu)));
		
		_items.Add(new GUIButton("Quit", buttonWidth, buttonHeight, new GUIButton.SetterBool(gui.setQuitGame)));
	}
	
	public override void draw ()
	{
		base.draw ();
		
		float areaWidth = 680f * GUIManager.widthCoef;
		float areaHeight = 800f * GUIManager.heightCoef;
		float areaX = 625f * GUIManager.widthCoef;
		float areaY = 225f * GUIManager.heightCoef;
		
		GUI.Label(new Rect(0f, 0f, Screen.width, Screen.height), "", "loadingBackground");
		GUILayout.BeginArea(new Rect(areaX, areaY, areaWidth, areaHeight));
		
		_scroolPos = GUILayout.BeginScrollView(_scroolPos, "scrollView", "scrollView");
		GUILayout.BeginVertical();
		
		GUILayout.FlexibleSpace();
		
		foreach(GUIItem itm in _items)
		{
			itm.draw();
			GUILayout.FlexibleSpace();
		}
		
		GUILayout.EndVertical();
		GUILayout.EndScrollView();
		
		GUILayout.EndArea();
	}
}
