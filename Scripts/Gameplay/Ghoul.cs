using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Ghoul : Creature {
	
	public override void Awake()
	{
		_type = Type.Ghoul;
		_preyedTypes = new List<Type>(){Type.Human};
		_eatenScale = new Vector3(0.1f, 1f, 0.1f);
	}
}
