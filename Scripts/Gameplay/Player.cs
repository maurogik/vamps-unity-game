using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class Player : Creature {

	public enum PowerType{
		BAT_DASH,
		THIRD_EYE,
		GHOST_SHAPE
	}
	
	private Dictionary<PowerType, Power> _powers;
	
	private float _baseDashDuration = 1f;
	private float _baseDashCooldown = 6f;
	private float _dashDurationModifier = 2f;
	private float _dashCooldownModifier = 2f;
	
	private float _baseEyeDuration = 10f;
	private float _baseEyeCooldown = 45f;
	//private float _eyeDurationModifier = 1f;
	private float _eyeCooldownModifier = 1f;
	
	private float _baseGhostDuration = 10f;
	private float _baseGhostCooldown = 10f;
	private float _ghostDurationModifier = 0.5f;
	private float _ghostCooldownModifier = 0.5f;
	
	private float _scalePower = 3f;
	
	private bool _isHolding = false;
	private Creature _heldCreature;
	
	public Material ghostMatHead;
	public Material ghostMatBody;
	public GUISkin uiskin;
	public float holdingDistance = 5f;
	
	
	
	public override void Awake()
	{
		_type = Type.Player;
		_preyedTypes = new List<Type>(){Type.Ghost, Type.Ghoul, Type.Human, Type.Vampire, Type.Cleric};
	}
	
	public override void Start ()
	{
		
		base.Start ();
	}

	public override void Update()
	{
		base.Update();
		
		//power part
		foreach(Power pow in _powers.Values)
		{
			pow.update(Time.deltaTime);
		}
		
		if(_heldCreature == null)
		{
			_isHolding = false;	
		}
		
		if(_isHolding && _heldCreature)
		{
			Vector3 creaturePos = transform.position + transform.forward * holdingDistance;
			creaturePos.y += _heldCreature.scale.y - scale.y * 0.8f;
			_heldCreature.transform.position = creaturePos;
			
			if(_heldCreature.isDead())
			{
				_heldCreature.GetComponent<AICreatureControl>().kill();
			}
		}
	}
	
	
	public void initPowers()
	{
		float scaleFactor = Mathf.Pow(scale.y, _scalePower);
		_powers = new Dictionary<PowerType, Power>();
		
		_powers[PowerType.BAT_DASH] = new BatDash(_baseDashDuration * Mathf.Pow(_dashDurationModifier, scaleFactor) ,
			_baseDashCooldown /(Mathf.Pow( _dashCooldownModifier, scaleFactor)));
		
		_powers[PowerType.THIRD_EYE] = new ThirdEye(_baseEyeDuration * scaleFactor,
			_baseEyeCooldown /( _eyeCooldownModifier * scaleFactor));
		
		_powers[PowerType.GHOST_SHAPE] = new GhostShape(_baseGhostDuration * _ghostDurationModifier * scaleFactor,
			_baseGhostCooldown /( _ghostCooldownModifier * scaleFactor));
		
	}
	
	private void reinitPowers()
	{
		float scaleFactor = Mathf.Pow(scale.y, _scalePower);
		
		_powers[PowerType.BAT_DASH].reset(_baseDashDuration * Mathf.Pow(_dashDurationModifier, scaleFactor) ,
			_baseDashCooldown /(Mathf.Pow( _dashCooldownModifier, scaleFactor)));
		
		_powers[PowerType.THIRD_EYE].reset(_baseEyeDuration * scaleFactor,
			_baseEyeCooldown /( _eyeCooldownModifier * scaleFactor));
		
		_powers[PowerType.GHOST_SHAPE].reset(_baseGhostDuration * _ghostDurationModifier * scaleFactor,
			_baseGhostCooldown /( _ghostCooldownModifier * scaleFactor));
	}
	
	public void activatePower(PowerType type)
	{
		foreach(Power pow in _powers.Values)
		{
			if(pow.isActivated() && pow.Type != type)
			{
				return;	
			}
		}
		
		EventHandler eventHandler = GetComponent<EventHandler>();
		if(!eventHandler.enabled)
		{
			eventHandler.enabled = true;
		}
		
		_powers[type].activate();
	}
	
	public void deactivatePower(PowerType type)
	{
		Debug.Log("try to deactivate : "+type.ToString());
		_powers[type].pause();
	}
	
	public bool isActivated(PowerType type)
	{
		return _powers[type].isActivated();
	}
	
	public override bool eat(Vector3 creaScale)
	{
		bool res = false;

		scale.x += _scaleModifier * creaScale.x;
		scale.z += _scaleModifier * creaScale.z;
		scale.y += _scaleModifier  * creaScale.y;
		res = true;
			
		reinitPowers();
		return res;
	}

	
	public bool isPowerRunnng()
	{
		foreach(Power pow in _powers.Values)
		{
			if(pow.isActivated())
			{
				return true;	
			}
		}
		
		return false;
	}
	
	
	public void OnGUI()
	{
		GUISkin bkup = GUI.skin;
		GUI.skin = uiskin;

		Color bkupCol = GUI.color;
		
		GUI.color = getColorFromHealth();
		GUI.Label(new Rect(Screen.width - 230f, 30f, 200f, 300f), "", "ScaleUIBack");
		
		GUI.color = bkupCol;
		
		GUILayout.BeginArea(new Rect(Screen.width - 330f, Screen.height - 230f , 300f, 200f));
		GUILayout.BeginVertical();
		
		GUILayout.FlexibleSpace();
		
		foreach(Power power in _powers.Values)
		{
			power.guiPower();	
		}
		
		GUILayout.FlexibleSpace();
		
		GUILayout.EndVertical();
		GUILayout.EndArea();
		
		GUI.skin = bkup;
	}
	
	
	private Color getColorFromHealth()
	{
		float r,g,b;
		
		float ratioHeight = scale.y / scale.z; 
		r = Mathf.Max(1f, ratioHeight);
		g = 1f;
		b = 1f / Mathf.Min(1f, scale.y);
		
		float max = Mathf.Max (r, g, b);
		r /= max;
		g /= max;
		b /= max;
		
		return new UnityEngine.Color(r, g, b, 1f);
	}
	
	
	public override bool isDead()
	{
		return	(scale.y < _deadThresold || scale.x/scale.y < _deadThresold) && !isPowerRunnng();
	}
	
	public bool isHolding()
	{
		return _isHolding;
	}
	
	public void startHolding(Creature crea)
	{
		if(isPreyingOn(crea.CreatureType))
		{
			_heldCreature = crea;
			_heldCreature.GetComponent<AICreatureControl>().enabled = false;
			_isHolding = true;
			_heldCreature.transform.rotation = Quaternion.Euler(Vector3.zero);
		}
	}
	
	public void releaseHold()
	{
		if(_heldCreature != null)
		{
			_heldCreature.GetComponent<Creature>().enabled = true;
			_heldCreature.GetComponent<AICreatureControl>().enabled = true;
			_heldCreature = null;
		}
		_isHolding = false;
	}

}
