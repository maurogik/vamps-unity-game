using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {
	
	
	private float _timePauseVertical = 0.3f;
	private float _timePauseHorizontal = 0.1f;
	private float _lastUIHorizontal = 0f;
	private float _lastUIVertical = 0f;
	private static InputManager _input;
	
	public float getCameraX()
	{
		float val = Input.GetAxis("CamHorizontalKeyboard");
		if(val == 0f)
		{
			val = Input.GetAxis("CamHorizontalJoystick");
			
			if(val == 0f)
			{
				val = Input.GetAxis("CamHorizontalMouse");
			}
		}
		
		return val;
	}
	
	public float getCameraY()
	{
		float val = Input.GetAxis("CamVerticalKeyboard");
		if(val == 0f)
		{
			val = Input.GetAxis("CamVerticalJoystick");
			
			if(val == 0f)
			{
				val = Input.GetAxis("CamVerticalMouse");
			}
		}
		
		return val;
	}
	
	public float getUIVertical()
	{
		float res = 0f;
		if( Time.time - _lastUIVertical > _timePauseVertical)
		{
			_lastUIVertical = Time.time;
			
			res = Input.GetAxis("Vertical");
			if(res == 0f)
			{
				res = Input.GetAxis("CamVerticalJoystick");
				if(res == 0f)
				{
					res = Input.GetAxisRaw("CamVerticalKeyboard");
				}
			}
		}
		
		return res;
	}
	
	public float getUIHorizontal()
	{
		float res = 0f;
		if( Time.time - _lastUIHorizontal > _timePauseHorizontal)
		{
			_lastUIHorizontal = Time.time;
			
			res = Input.GetAxis("Horizontal");
			if(res == 0f)
			{
				res = Input.GetAxis("CamHorizontalJoystick");
				if(res == 0f)
				{
					res = Input.GetAxisRaw("CamHorizontalKeyboard");
				}
			}
		}
		
		return res;
	}
	
	public bool getUIValidate()
	{
		bool res;
		res = getButton("Jump");
		res |= Input.GetKey(KeyCode.Return);
		
		return res;
	}
	
	public float getMouvementX()
	{
		return Input.GetAxis("Horizontal");
	}
	
	public float getMouvementY()
	{
		return Input.GetAxis("Vertical");
	}
	
	public bool getButtonDown(string buttonName)
	{
		return Input.GetButtonDown(buttonName);
	}
	
	public bool getButton(string buttonName)
	{
		return Input.GetButton(buttonName);
	}
	
	// Use this for initialization
	void Awake () {
		_input = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static InputManager input {
		get {
			return _input;
		}
	}
}
