using UnityEngine;
using System.Collections;


public class CoffinHandler : MonoBehaviour {


	public float getInDuration = 2f;
	public float getOutDuration = 2f;
	public float minStayInDuration = 3f;
	public float maxStayInDuration = 8f;
	public Transform target;
	
	private Transform _currentVamp;
	private float _stayInDuration;
	private float _timer;
	private Vector3 _originalPos;
	
	void OnEnable()
	{
	}
	
	void OnDisable()
	{
		_currentVamp = null;
	}
	
	public void setOccupant(Transform vamp)
	{
		_currentVamp = vamp;
		_originalPos = vamp.position;
		_timer = 0f;
		
		float minStay, maxStay;
		
		DayNightController worldTimer = GameObject.Find("Core").GetComponent<WorldController>().worldTime;
		float hour = worldTimer.currentCycleTime;
		
		//night
		if(hour < worldTimer.dayCycleLength && hour > worldTimer.DuskTime)
		{
			minStay = minStayInDuration;
			maxStay = maxStayInDuration;
		}
		//day
		else
		{
			hour = worldTimer.DuskTime - hour;
			minStay = hour/1.5f;
			maxStay = hour;
		}
		
		_stayInDuration = Random.Range(minStay, maxStay);
		_currentVamp.GetComponent<AICreatureControl>().enabled = false;
		_currentVamp.GetComponent<Creature>().enabled = false;
	}
	
	
	// Update is called once per frame
	void Update ()
	{
		_timer += Time.deltaTime;
		
		if(_currentVamp == null)
		{
			enabled = true;	
			return;
		}
		
		if(_timer <= getInDuration)
		{
			_currentVamp.position = Vector3.Lerp(_originalPos, target.position, _timer/getInDuration);
		}
		else if(_timer <= getInDuration + _stayInDuration)
		{
			_currentVamp.gameObject.SetActive(false);
		}
		else if(_timer <= getInDuration + _stayInDuration + getOutDuration)
		{
			_currentVamp.gameObject.SetActive(true);
			_currentVamp.position = Vector3.Lerp(target.position, _originalPos,
				(_timer - _stayInDuration - getInDuration)/getOutDuration);
		}
		else
		{
			_currentVamp.position = _originalPos;
			_currentVamp.GetComponent<AICreatureControl>().enabled = true;
			Creature occupingCrea = _currentVamp.GetComponent<Creature>();
			occupingCrea.enabled = true;
			occupingCrea.eat(Vector3.one);
			enabled = false;
		}
	}

}
