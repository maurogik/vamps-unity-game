using UnityEngine;
using System.Collections;

public class TextMenu : GUIMenu {

	string _text;
	
	public TextMenu(string text)
	{
		_text = text;
		
		float buttonHeight = 100f;
		float buttonWidth = 680f;
		
		GUIManager gui = GameObject.Find("Core").GetComponent<GUIManager>();
		
		_items.Add(new GUIButton("Back", buttonWidth, buttonHeight, new GUIButton.SetterBool(gui.setMainMenu)));
	}
	
	
	public override void draw ()
	{
		base.draw ();
		
		float areaWidth = 860f * GUIManager.widthCoef;
		float areaHeight = 980f * GUIManager.heightCoef;
		float areaX = 530f * GUIManager.widthCoef;
		float areaY = 67f * GUIManager.heightCoef;
		
		GUI.Label(new Rect(0f, 0f, Screen.width, Screen.height), "", "textBackground");
		GUILayout.BeginArea(new Rect(areaX, areaY, areaWidth, areaHeight));
		
		_scroolPos = GUILayout.BeginScrollView(_scroolPos, "scrollView", "scrollView");
		GUILayout.BeginVertical();
		
		GUILayout.FlexibleSpace();
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		GUILayout.Label(_text, "simpleText");
		
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.FlexibleSpace();
		
		foreach(GUIItem itm in _items)
		{
			itm.draw();
		}

		GUILayout.FlexibleSpace();
		
		GUILayout.EndVertical();
		GUILayout.EndScrollView();
		
		GUILayout.EndArea();
	}
}
